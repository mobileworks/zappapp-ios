//
//  AlertMessage.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation
import UIKit

class ActivityIndicator
{
    private let activityView: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func setupForView(_ delegate: UIView) {
        activityView.center = delegate.center
        activityView.hidesWhenStopped = true
        activityView.frame = CGRect(x: 0, y: 0, width: delegate.bounds.width, height: delegate.bounds.height)
        activityView.backgroundColor = UIColor(red: 0.27, green: 0.27, blue: 0.27, alpha: 0.7)
        activityView.style = UIActivityIndicatorView.Style.whiteLarge
        delegate.addSubview(activityView)
    }
    
    func start(){
        activityView.startAnimating()
    }
    
    func stop(){
        activityView.stopAnimating()
    }
}
