//
//  EnquiryPicturesViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 05.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import RealmSwift

class EnquiryPicturesViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PhotoGaleryDelegate {
    
    var list = [ImageData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.center = self.view.center
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.setProgress(0.5, animated: false)
        self.view.addSubview(progressView)
        
        NSLayoutConstraint.activate([
            progressView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            progressView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 6),
        ])

        let realm = try! Realm()
        if let property = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.MC3_ENQUIRY, key: "attachments", realm) {
            
            let docGuid: String? = nil
            list = getPhotosList(docGuid: docGuid, property: property)
            let stackView = CustomPhotoGalleryPopOverView(controller: self, property: property, documentGuid: docGuid, list: list)
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.delegate = self
            self.view.addSubview(stackView)
            
            NSLayoutConstraint.activate([
                stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                stackView.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 6),
                stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
        }
    }
   
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    // MARK: - Navigation

    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButton(_ sender: UIBarButtonItem) {
        saveEnquiry()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnquiryLocationController")
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func saveEnquiry() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let enquiry = appDelegate.enquiry
        enquiry.enquiryPhotos = list
    }
    
    func getPhotos(photos: [ImageData]?) {
        list = photos!
    }
    
    func getPhotosList(docGuid: String?, property: DocumentProperty) -> [ImageData] {
        var list = [ImageData]()
        if docGuid != nil {
            let realm = try! Realm()
            if let dpData = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.MC3_ENQUIRY_ATTACHMENT, key: PropertiesConsts.DATA, realm) {
                if let photosList = DocumentPropertyValue.getDocumentPropertyValue(propertyId: property.id, documentGuid: docGuid!, realm) {

                    for photo in photosList.documents {
                        if let encodedString = DocumentPropertyValue.getDocumentPropertyValue(propertyId: dpData.id, documentGuid: photo.guid!, realm) {
                            let imageData = ImageData()
                            let imageData2 = NSData(base64Encoded: (encodedString.content)!, options: [])

                            let image = UIImage(data: imageData2! as Data)
                            let imageView = UIImageView()
                            imageView.image = image

                            imageData?.imageView = imageView

                            let formatter = DateFormatter()
                            formatter.dateFormat = Constants.PHOTO_DATETIME_FORMAT

                            imageData?.date = formatter.string(from: Date())
                            imageData?.isReadOnly = true
                            imageData?.isFromDisk = false

                            list.append(imageData!)
                        }
                    }
                }
            }
        }
        return list
    }
}
