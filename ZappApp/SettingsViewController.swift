//
//  SettingsViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 17.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var pickerData: [String] = [String]()
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    var reporterGuid = ""
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let titlePickerView = UIPickerView()
    let firstNameTextField: UITextField = {
        let v = UITextField()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.borderStyle = .roundedRect
        return v
    }()
    let lastNameTextField: UITextField = {
        let v = UITextField()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.borderStyle = .roundedRect
        return v
    }()
    let emailTextField: UITextField = {
        let v = UITextField()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.borderStyle = .roundedRect
        return v
    }()
    let phoneNoTextField: UITextField = {
        let v = UITextField()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.borderStyle = .roundedRect
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        activityIndicator.setupForView(self.view)
        self.view.addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            scrollView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        let mainView = UIStackView()
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.axis = .vertical
        mainView.distribution = .equalSpacing
        mainView.alignment = .top
        mainView.spacing = 8
        
        scrollView.addSubview(mainView)
        
        NSLayoutConstraint.activate([
            mainView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 8),
            mainView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 8),
            mainView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -16),
            mainView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 8),
        ])
        
        titlePickerView.translatesAutoresizingMaskIntoConstraints = false
        titlePickerView.frame = CGRect(x: 0, y: 0, width: mainView.bounds.size.width, height: 90)
        titlePickerView.dataSource = self
        titlePickerView.delegate = self
        mainView.addArrangedSubview(titlePickerView)
        
        NSLayoutConstraint.activate([
            titlePickerView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            titlePickerView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
        ])
        
        firstNameTextField.placeholder = "First name"
        mainView.addArrangedSubview(firstNameTextField)
        NSLayoutConstraint.activate([
            firstNameTextField.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            firstNameTextField.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
        ])
        
        firstNameTextField.placeholder = "Last name"
        mainView.addArrangedSubview(lastNameTextField)
        NSLayoutConstraint.activate([
            lastNameTextField.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            lastNameTextField.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
        ])
        
        emailTextField.isEnabled = false
        emailTextField.backgroundColor = UIColor.lightGray
        mainView.addArrangedSubview(emailTextField)
        NSLayoutConstraint.activate([
            emailTextField.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            emailTextField.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
        ])
        
        phoneNoTextField.placeholder = "Telephone no"
        mainView.addArrangedSubview(phoneNoTextField)
        NSLayoutConstraint.activate([
            phoneNoTextField.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            phoneNoTextField.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
        ])
        
        let realm = try! Realm()
        if let titleProperty = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.CRM_CONTACT, key: PropertiesConsts.TITLE, realm) {
            pickerData = getEnumData(titleProperty, realm: realm)
        }
        
        reporterGuid = UserDefaults.standard.string(forKey: PropertiesConsts.REPORTER_GUID)!
        firstNameTextField.text = UserDefaults.standard.string(forKey: PropertiesConsts.USER_FIRST_NAME)
        lastNameTextField.text = UserDefaults.standard.string(forKey: PropertiesConsts.USER_LAST_NAME)
        emailTextField.text = UserDefaults.standard.string(forKey: PropertiesConsts.USER_EMAIL)
        phoneNoTextField.text = UserDefaults.standard.string(forKey: PropertiesConsts.USER_PHONE_NO)
        
        let picker = UserDefaults.standard.string(forKey: PropertiesConsts.USER_TITLE)
        for (index, element) in pickerData.enumerated() {
            if element == picker {
                titlePickerView.selectRow(index, inComponent: 0, animated: true)
            }
        }
        
        let buttonLogout = UIButton()
        buttonLogout.setTitleColor(buttonLogout.tintColor, for: .normal)
        buttonLogout.setTitle("Logout", for: UIControl.State.normal)
        buttonLogout.translatesAutoresizingMaskIntoConstraints = false
        buttonLogout.addTarget(self, action:#selector(logout), for:.touchUpInside)
        mainView.addArrangedSubview(buttonLogout)
        
        NSLayoutConstraint.activate([
            buttonLogout.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
        ])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                if endFrame.origin.y >= UIScreen.main.bounds.size.height {
                    self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.size.width, height: self.scrollView.bounds.size.height)
                } else {
                    self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.size.width, height: self.scrollView.bounds.size.height + endFrame.size.height)
                }
            }
        }
    }
    
    func getEnumData(_ property: DocumentProperty, realm: Realm) -> [String] {
        var labels = [String]()
        if let dataFromString = property.params?.data(using: .utf8, allowLossyConversion: false) {
            do {
                let json = try JSON(data: dataFromString)
                
                let groupsArray:[String] = json.arrayValue.map { $0.string!}
                for object in groupsArray {
                    if object == "" {
                        labels.append("   ")
                    } else {
                        labels.append(object)
                    }
                }
            } catch {
                print(error)
            }
        }
        return labels
    }
    
    //MARK: PickerViewDelegate
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        
        var cancel = false
        
        let userFirstName = firstNameTextField.text!
        let userLastName = lastNameTextField.text!
        let userPhoneNo = phoneNoTextField.text!
        
        // Check if title was choosen
        let userTitle = pickerData[titlePickerView.selectedRow(inComponent: 0)]
        if titlePickerView.selectedRow(inComponent: 0) == 0 {
            titlePickerView.layer.borderColor = UIColor.red.cgColor
            cancel = true
        } else {
            titlePickerView.layer.borderColor = UIColor.clear.cgColor
        }
        
        // Check for a valid user first name.
        if userFirstName.isEmpty {
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
            cancel = true
        } else {
            firstNameTextField.layer.borderColor = UIColor.clear.cgColor
        }
        
        // Check for a valid user last name.
        if userLastName.isEmpty {
            lastNameTextField.layer.borderColor = UIColor.red.cgColor
            cancel = true
        } else {
            lastNameTextField.layer.borderColor = UIColor.clear.cgColor
        }
        
        if !cancel {
            attempt2Save(userTitle: userTitle, userFirstName: userFirstName, userLastName: userLastName, userPhoneNo: userPhoneNo)
        }
    }
    
    func attempt2Save(userTitle: String, userFirstName: String, userLastName: String, userPhoneNo : String) {
        
        activityIndicator.start()
        self.saveProfile(userTitle: userTitle, userFirstName: userFirstName, userLastName: userLastName, userPhoneNo: userPhoneNo) { (status) in
            switch(status) {
            case 0,
                 1:
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stop()
                    AlertMessage.displayAlertMessage(self, title: "Error", message: "Error when saving profile", actionLabel: "ok".localized)
                })
            case 2:
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stop()
                    AlertMessage.displayAlertMessage(self, title: "ok".localized, message: "Changes saved", actionLabel: "ok".localized)
                })
            default:
                break
            }
        }
    }
    
        
    func saveProfile(userTitle: String, userFirstName: String, userLastName: String, userPhoneNo : String, completion:@escaping (_ status : Int) -> ()) {
        var json = Parameters()
        let realm = try! Realm()
        
        var properties = [Any]()
        
        for property in DocumentProperty.getDocumentPropertiesByDocumentType(docType: DocTypeConsts.CRM_CONTACT, realm) {
            switch property.key! {
            case PropertiesConsts.TITLE:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value:userTitle)
            case PropertiesConsts.FIRST_NAME:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: userFirstName)
             case PropertiesConsts.LAST_NAME:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: userLastName)
            case PropertiesConsts.PHONE_NUMBER:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: userPhoneNo)
            default:
                break
            }
        }
        
        json[PropertiesConsts.TYPE] = DocTypeConsts.CRM_CONTACT
        json[PropertiesConsts.TIME] = NSDate().timeIntervalSince1970
        json[PropertiesConsts.PROPERTIES] = properties
        
        let updateProfileThread = DispatchQueue(label: "updateProfile", qos: .background, target: nil)
        updateProfileThread.async {
        
            ApiDocument().update(content: json, endPoint: Config.API.Operations.document.endpoint+"/"+self.reporterGuid, senderParam: 0, queue: updateProfileThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished{
                    let realm = try! Realm()
                    for (key, doc):(String, JSON) in response! {
                        if key == SyncConsts.DOCUMENT {
                            if doc[SyncConsts.GUID].string != nil {
                                //SharedSync.parseDocument(response!, onlyInsert: false)
                                let propertiesValues = doc[SyncConsts.PROPERTIES_VALUES].arrayValue
                                for key in propertiesValues {
                                    if key[PropertiesConsts.KEY].string != nil {
                                        for values in DocumentPropertyValue.getDpvByDocument(documentGuid: self.reporterGuid, realm) {
                                            if let property = DocumentProperty.getDocumentPropertyById(id: values.propertyId, realm) {
                                                let key = property.key!
                                                switch key {
                                                case PropertiesConsts.TITLE:
                                                     UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_TITLE)
                                                case PropertiesConsts.FIRST_NAME:
                                                    UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_FIRST_NAME)
                                                case PropertiesConsts.LAST_NAME:
                                                    UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_LAST_NAME)
                                                case PropertiesConsts.PHONE_NUMBER:
                                                    UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_PHONE_NO)
                                                default:
                                                    break
                                                }
                                                UserDefaults.standard.synchronize()
                                            }
                                        }
                                        completion(2)
                                    }
                                }
                            }
                        } else {
                            completion(1)
                        }
                    }
                } else {
                    completion(0)
                }
            })
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func logout() {
        let ac = UIAlertController(title: "Do you want to logout ?", message: nil, preferredStyle: .alert)
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default) {(action: UIAlertAction!) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.loggedIn = false
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.present(storyboard.instantiateViewController(withIdentifier: "LoginRegister"), animated: true, completion: nil)
        }
        ac.addAction(logoutAction)
        
        let cancelAction = UIAlertAction(title: "cancel".localized, style: .default, handler: {
            (alertAction:UIAlertAction!) in ac.dismiss(animated: true, completion: nil)
        })
        ac.addAction(cancelAction)
        
        present(ac, animated: true, completion: nil)
    }
}
