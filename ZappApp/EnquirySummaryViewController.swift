//
//  ViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 08.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class EnquirySummaryViewController: UIViewController {

    var enquiryGuid = ""
    
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setupForView(self.view)
        
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0).cgColor
        textView.layer.cornerRadius = 5.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func completeEnquiry(_ sender: UIBarButtonItem) {
        saveEnquiry("\(textView.text!)")
        activityIndicator.start()
        sendEnquiry({status in
            switch status {
            case 2:
                self.sendPhotos(self.enquiryGuid, completion: ({status in
                    if status {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.enquiry = NewEnquiry()
                        
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stop()
                            AlertMessage.displayAlertMessage(self, title: "Success", message: "New enquiry was sent", actionLabel: "ok".localized, uiView: TabBarMainController())
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stop()
                            AlertMessage.displayAlertMessage(self, title: "Error", message: "Error when sending photo", actionLabel: "ok".localized, uiView: TabBarMainController())
                        })
                    }
                }))
            case 1,0:
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stop()
                    AlertMessage.displayAlertMessage(self, title: "Error", message: "Error when sending new enquiry", actionLabel: "ok".localized, uiView: TabBarMainController())
                })
            default:
                break
            }
        })
    }
    
    func saveEnquiry(_ enquiryComment : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let enquiry = appDelegate.enquiry
        enquiry.enquiryComment = enquiryComment
    }
    
    func sendEnquiry(_ completion:@escaping (_ status : Int) -> ()) {
        var json = Parameters()
        var properties = [Any]()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let enquiry = appDelegate.enquiry
        
        let realm = try! Realm()
        
        for property in DocumentProperty.getDocumentPropertiesByDocumentType(docType: DocTypeConsts.MC3_ENQUIRY, realm) {
            switch property.key! {
            case PropertiesConsts.LOCATION:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: enquiry.enquiryLocation)
            case PropertiesConsts.ENQUIRY_TYPE:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: enquiry.enquiryTypeName)
            case PropertiesConsts.REPORTER:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: UserDefaults.standard.string(forKey: PropertiesConsts.REPORTER_GUID)!)
            case PropertiesConsts.REPORTER_TYPE:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: PropertiesConsts.PUBLIC)
            case PropertiesConsts.ADDRESS:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: enquiry.enquiryAddress)
            case PropertiesConsts.STATUS:
                if let dpStatus = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.MC3_ENQUIRY_STATUS, key: PropertiesConsts.KEY, realm) {
                    if let dpvStatus = DocumentPropertyValue.getDocumentPropertyValueByPropertyGuidContent(propertyId: dpStatus.id, content: PropertiesConsts.MC3_ENQUIRY_STATUS_NEW, realm) {
                        properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: dpvStatus.documentGuidFromDpv()!)
                    }
                }
            case PropertiesConsts.SLA:
                if let dpSla = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.MC3_ENQUIRY_SLA, key: PropertiesConsts.KEY, realm) {
                    if let dpvSla = DocumentPropertyValue.getDocumentPropertyValueByPropertyGuidContent(propertyId: dpSla.id, content: PropertiesConsts.MC3_ENQUIRY_SLA_10_DAYS_STANDARD, realm) {
                        properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: dpvSla.documentGuidFromDpv()!)
                    }
                }
            case PropertiesConsts.SOURCE:
                if let dpSource = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.MC3_ENQUIRY_SOURCE, key: PropertiesConsts.KEY, realm) {
                    if let dpvSource = DocumentPropertyValue.getDocumentPropertyValueByPropertyGuidContent(propertyId: dpSource.id, content: PropertiesConsts.MC3_ENQUIRY_SOURCE_ZAPPAPP, realm) {
                        properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: dpvSource.documentGuidFromDpv()!)
                    }
                }
            case PropertiesConsts.NOTES:
                if enquiry.enquiryComment != "" {
                    properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: enquiry.enquiryComment)
                }
            default:
                break
            }
        }
        
        json[PropertiesConsts.TYPE] = DocTypeConsts.MC3_ENQUIRY
        //json[PropertiesConsts.TIME] = NSDate().timeIntervalSince1970
        json[PropertiesConsts.PROPERTIES] = properties
        
        let profileThread = DispatchQueue(label: "profile", qos: .background, target: nil)
        profileThread.async {
            ApiDocument().create(content: json, endPoint: Config.API.Operations.document.endpoint, senderParam: 0, queue: profileThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished {
                    for (key, doc):(String, JSON) in response! {
                        if key == SyncConsts.DOCUMENT {
                            if let enqGuid = doc[SyncConsts.GUID].string {
                                self.enquiryGuid = enqGuid
                                completion(2)
                            }
                        } else {
                            completion(1)
                        }
                    }
                } else {
                    completion(0)
                }
            })
        }
    }
    
    func sendPhotos(_ enquiryGuid: String, completion:@escaping (_ status : Bool) -> ()) {
        var json = Parameters()
        
        var properties = [Any]()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let enquiry = appDelegate.enquiry
        
        if enquiry.enquiryPhotos.count == 0 {
            completion(true)
        }
        
        let realm = try! Realm()
        
        for photo in enquiry.enquiryPhotos {
            for docProperty in DocumentProperty.getDocumentPropertiesByDocumentType(docType: DocTypeConsts.MC3_ENQUIRY_ATTACHMENT, realm)  {
                switch docProperty.key! {
                case PropertiesConsts.NAME:
                    properties = DocumentParser.add2PropertiesArray(properties, key: docProperty.key!, value: photo.date!)
                case PropertiesConsts.DATA:
                    properties = DocumentParser.add2PropertiesArray(properties, key: docProperty.key!, value: (photo.imageView?.image?.base64(.jpeg(90)))!)
                case PropertiesConsts.RELATED_DOCUMENTS:
                    var documents = [Any]()
                    documents.append(enquiryGuid)
                    properties = DocumentParser.add2PropertiesArray(properties, key: docProperty.key!, value: documents)
                default:
                    break
                }
            }
        
            json[PropertiesConsts.TYPE] = DocTypeConsts.MC3_ENQUIRY_ATTACHMENT
            json[PropertiesConsts.TIME] = NSDate().timeIntervalSince1970
            json[PropertiesConsts.PROPERTIES] = properties

            let photoThread = DispatchQueue(label: "photo", qos: .background, target: nil)
            photoThread.async {
                ApiDocument().create(content: json, endPoint: Config.API.Operations.document.endpoint, senderParam: 0, queue: photoThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                    if finished{
                        for (key, doc):(String, JSON) in response! {
                            if key == SyncConsts.DOCUMENT {
                                let repGuid = doc[SyncConsts.GUID].string
                                if repGuid != nil {
                                    completion(true)
                                }
                            } else {
                                completion(false)
                            }
                        }
                    } else {
                        completion(false)
                    }
                })
            }
        }
    }
}
