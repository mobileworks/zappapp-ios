//
//  Config.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

struct Config {
    static let maxConnections = 10
    
    struct API {
        struct Instance{
            static let domain = "staging.maestrolive.biz"
            static let name = "demo"
            static let transferProtocol = "http"
            
            static let URL = "\(transferProtocol)://\(name).\(domain)"
        }
        
        struct Operations{
            struct server_list{
                static let JWTenabled = false;
                static let endpoint = "/server_list"
            }
            
            struct instances{
                static let JWTenabled = false;
                static let endpoint = "/api/v2/instances.json"
            }
            
            struct login{
                static let JWTenabled = false;
                static let endpoint = "/api/v2/login.json"
            }
            
            struct documentLastModified{
                static let JWTenabled = true;
                static let endpoint = "/api/v2/documentLastModified.json"
            }
            
            struct document{
                static let JWTenabled = true
                static let endpoint = "/api/v2.1/document.json"
            }
        }
    }
}
