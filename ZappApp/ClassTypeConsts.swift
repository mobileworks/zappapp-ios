//
//  ClassTypeConsts.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 06.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation

class ClassTypeConsts {
    
    //ClassTypes
    static let COMPUTED = "Computed\\"
    static let DOCUMENT_PROPERTY_VALUE_CONTENT = "Document\\PropertyValue\\Content\\"
    
    //Core
    static let CORE = "\\Core\\\(DOCUMENT_PROPERTY_VALUE_CONTENT)"
    static let BOOLEAN = "\(CORE)Boolean"
    static let DOCUMENT = "\(CORE)Document"
    static let DURATION = "\(CORE)Duration"
    static let ENUMERATE = "\(CORE)Enumerate"
    static let ENUMERATE_MULTIPLE = "\(CORE)EnumerateMultiple"
    static let DOCUMENTLIST = "\(CORE)DocumentList"
    static let DOCUMENTSINGLELIST = "\(CORE)DocumentSingleList"
    static let DOCUMENTORDEREDLIST = "\(CORE)DocumentOrderedList"
    static let LOCATION = "\(CORE)Location"
    static let PRIMITIVE = "\(CORE)Primitive"
    static let POLYLINE = "\(CORE)Polyline"
    static let POLYGON = "\(CORE)Polygon"
    static let ENTITY = "\(CORE)Entity"
    static let USER = "\(CORE)User"
    static let KEY = "\(CORE)Key"
    static let BINARY = "\(CORE)Binary"
    static let DOCUMENTCREATED = "\(CORE)\(COMPUTED)DocumentCreated"
    static let DOCUMENTUPDATED = "\(CORE)\(COMPUTED)DocumentUpdated"
    static let DOCUMENTCREATOR = "\(CORE)\(COMPUTED)DocumentCreator"
    static let DOCUMENTWORKFLOWSTATE = "\(CORE)\(COMPUTED)DocumentWorkflowState"
    static let DOCUMENTPROPERTIESCHAIN = "\(CORE)\(COMPUTED)DocumentPropertiesChain"
    static let DATETIME = "\(CORE)DateTime"
    static let DATE = "\(CORE)Date"
    static let SIGNATURE = "\(CORE)Signature"
    static let JSON_ARRAY = "\(CORE)JsonArray"
    
    //Office
    static let OFFICE = "\\Office\\\(DOCUMENT_PROPERTY_VALUE_CONTENT)"
    static let TASK_ASSIGNEE = "\(OFFICE)\(COMPUTED)TaskAssignee"
    static let SCHEDULE_AVAILABILITY = "\(OFFICE)\(COMPUTED)ScheduleAvailability"
    static let RELATED_PROBLEM_LOCATION = "\(OFFICE)\(COMPUTED)RelatedProblemLocation"
    static let RELATED_PROBLEM = "\(OFFICE)\(COMPUTED)RelatedProblem"
    static let PROBLEM_SLA = "\(OFFICE)\(COMPUTED)ProblemSLA"
}
