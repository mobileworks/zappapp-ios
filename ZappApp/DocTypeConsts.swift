//
//  DocTypeConsts.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 18.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation

class DocTypeConsts {
    
    static let CRM = "CRM"
    static let MC3 = "MC3"
    
    static let CRM_CONTACT = "crm.contact"
    static let CRM_BUSINESS = "crm.business"
    static let CRM_BUSINESS_CATEGORY = "crm.busines.category"

    static let MC3_ENQUIRY = "mc3.enquiry"
    static let MC3_ENQUIRY_TYPE = "mc3.enquiry.type"
    static let MC3_ENQUIRY_RAG_PERIOD = "mc3.enquiry.ragPeriod"
    static let MC3_ENQUIRY_SLA = "mc3.enquiry.sla"
    static let MC3_ENQUIRY_SOURCE = "mc3.enquiry.source"
    static let MC3_ENQUIRY_STATUS = "mc3.enquiry.status"
    static let MC3_ENQUIRY_ATTACHMENT = "mc3.enquiry.attachment"
    static let MC3_ENQUIRY_CATEGORY = "mc3.enquiry.category"
    static let MC3_ENQUIRY_DEPARTMENT = "mc3.enquiry.department"
    
    static let OFFICE_SETTINGS_TIMEZONE = "office.settings.timezone"
}
