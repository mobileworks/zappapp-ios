//
//  Enquiry.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 03.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit

class PickerViewObject {
    
    // MARK: Properties
    var key: String
    var value: String
    
    // MARK: Initialization
    
    init?(key: String, value: String) {
        // Initialize stored properties.
        self.key = key
        self.value = value
    }
}
