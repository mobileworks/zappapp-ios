//
//  MapPoint.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 29.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit
import MapKit

class MapPoint: NSObject, MKAnnotation {
    let title: String?
    let address: String
    let enquiryType: String
    let date: String
    let coordinate: CLLocationCoordinate2D
    let image: UIImage
    
    init(address: String, enquiryType: String, date: String, coordinate: CLLocationCoordinate2D, image: UIImage) {
        self.title = ""
        self.address = address
        self.enquiryType = enquiryType
        self.date = date
        self.coordinate = coordinate
        self.image = image
        
        super.init()
    }
}
