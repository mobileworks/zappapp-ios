//
//  SharedSync.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 18.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import Alamofire

class SharedSync {
    
    static let noOfResults = 7500
    static let startFrom = 0
    
    static var docList = [Document]()
    var startFromList = [Int]()
    
    static let errorMessageNotificationKey = "uk.ltd.mobileworks.mango.error.message"
    static let showLoginViewNotificationKey = "uk.ltd.mobileworks.mango.show.login.view"
    
    func startSync(type: String, groups: String?, startFrom: Int, noOfResults: Int, maxUpdateTS: Int64, isAsset: Bool, completion2: @escaping (_ counter: SyncResult?) -> ()) {
        var params = Parameters()
        params[SyncConsts.TYPE] = type
        params[SyncConsts.LAST_MODIFIED] = String(maxUpdateTS)
        params[SyncConsts.START_FROM] = String(startFrom)
        params[SyncConsts.MAX_RESULTS] = String(noOfResults)
        params[SyncConsts.MOBILE] = "1"
        if isAsset {
            params[SyncConsts.IGNORE_DELETED] = "1"
        }
        
        switch(type) {
            case DocTypeConsts.MC3_ENQUIRY_SOURCE:
                params = SharedSync.syncFilter(params: params, key: PropertiesConsts.KEY, value: PropertiesConsts.MC3_ENQUIRY_SOURCE_ZAPPAPP)
            case DocTypeConsts.MC3_ENQUIRY_SLA:
                params = SharedSync.syncFilter(params: params, key: PropertiesConsts.KEY, value: PropertiesConsts.MC3_ENQUIRY_SLA_10_DAYS_STANDARD)
            case DocTypeConsts.MC3_ENQUIRY_TYPE:
                params = SharedSync.syncFilter(params: params, key: PropertiesConsts.SEEN_IN_ZAPP_APP, value: "1")
            default:
                break
        }
        let queue = DispatchQueue(label: "xx", attributes: .concurrent)
        queue.sync {
            autoreleasepool {
                ApiDocument().getList(params: params, endPoint: Config.API.Operations.document.endpoint, queue: queue, completion:{ (finished: Bool, response: JSON?) -> Void in
                    if finished {
                        let realm = try! Realm()
                        completion2(self.parseDocument(responseJSON: response!, serverLastModified: maxUpdateTS, docType: type, isTransactionInProgress: false, startFrom: startFrom, isAsset: false, realm: realm))
                    } else {
                        completion2(nil)
                    }
                })
            }
        }
    }
    
    static func syncFilter(params: Parameters, key: String, value: String) -> Parameters {
        var parameters = params
        let json : JSON = [key: [value]]
        let jsonStr = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonStr2 = jsonStr?.trimmingCharacters(in: .whitespacesAndNewlines)
        parameters[SyncConsts.FILTERS] = jsonStr2
        return parameters
    }
    
    static func syncSort(params: Parameters, key: String, value: String) -> Parameters {
        var parameters = params
        let json : JSON = [key: [value]]
        let jsonStr = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonStr2 = jsonStr?.trimmingCharacters(in: .whitespacesAndNewlines)
        parameters[SyncConsts.SORT] = jsonStr2
        return parameters
    }
    
    func getResponse(queueId: Int, response: JSON, realm: Realm) -> Bool {
        
        parseDocument(responseJSON: response, realm: realm)
        if response[SyncConsts.DOCUMENT].exists() {
            let responseObj = response[SyncConsts.DOCUMENT]
            if responseObj[SyncConsts.GUID].exists() {
                if queueId != 0 {
                    Queue.updateParentGuid(parentGuid: responseObj[SyncConsts.GUID].stringValue, queueMessageId: queueId, realm)
                }
            }
            
            if queueId != 0 {
                Queue.delete(id: queueId, realm)
            }
        } else if response[SyncConsts.VALIDATION_ERRORS].exists() {
            let validationErrorJson = response[SyncConsts.VALIDATION_ERRORS]
            if validationErrorJson != JSON.null {
                let message = ["message":validationErrorJson.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))]
                NotificationCenter.default.post(name: Notification.Name(rawValue: SharedSync.errorMessageNotificationKey), object: message)
                if queueId != 0 {
                    Queue.delete(id: queueId, realm)
                }
            }
        }
        return true
    }
    
    func parseDocument(responseJSON : JSON, realm: Realm) -> SyncResult {
        return parseDocument(responseJSON: responseJSON, serverLastModified: 0, docType: nil, isTransactionInProgress: false, startFrom: 0, isAsset: false, realm: realm)
    }
    
    func parseDocument(responseJSON : JSON, serverLastModified: Int64, docType: String?, isTransactionInProgress: Bool, startFrom: Int, isAsset: Bool, realm: Realm) -> SyncResult {
        let startTime = NSDate().timeIntervalSince1970
        var noOfDocuments = 0
        let countFromJson = responseJSON[SyncConsts.COUNT].intValue
        
        if responseJSON[SyncConsts.DOCUMENT].exists() {
            let responseObj = responseJSON[SyncConsts.DOCUMENT]
            
            var propertyKeysList = [String: DocumentProperty]()
            var docGuid: String?
            
            if responseObj.type == .dictionary {
                // check if JSON Object
                noOfDocuments = 1
                let document = responseObj as JSON
                
                if document[PropertiesConsts.TYPE].exists() {
                    if let documentType = document[PropertiesConsts.TYPE][PropertiesConsts.NAME].string {
                        if document[PropertiesConsts.GUID].exists() {
                            docGuid = document[PropertiesConsts.GUID].stringValue
                        }
                        if let docType = DocumentType.getDocumentType(docType: documentType, realm) {
                            var lastModified = docType.lastModified
                            propertyKeysList = getPropertiesForDocumentType(documentType: documentType, realm: realm)
                            lastModified = singleDocument(lastModified: lastModified, propertyKeysList: propertyKeysList, documentJson: document, isAsset: isAsset, realm: realm)
                            if docType.lastModified < lastModified {
                                docType.lastModified = lastModified
                            }
                        }
                        print("Total time spend on \(documentType) sync = \(NSDate().timeIntervalSince1970 - startTime) [s], no of documents = \((docGuid == nil) ? String(noOfDocuments) : docGuid!) started from = \(startFrom)")
                    }
                }
            } else if responseObj.type == .array {
                // check if JSON Array
                if isTransactionInProgress {
                    noOfDocuments = responseObj.count
                    if noOfDocuments > 0 {
                        for (_, document):(String, JSON) in responseObj {
                                if let documentType = document[PropertiesConsts.TYPE][PropertiesConsts.NAME].string {
                                    if document[PropertiesConsts.GUID].exists() {
                                        docGuid = document[PropertiesConsts.GUID].stringValue
                                    }
                                    if let docType = DocumentType.getDocumentType(docType: documentType, realm) {
                                        var lastModified = docType.lastModified
                                        propertyKeysList = getPropertiesForDocumentType(documentType: documentType, realm: realm)
                                        lastModified = singleDocument(lastModified: lastModified, propertyKeysList: propertyKeysList, documentJson: document, isAsset: isAsset, realm: realm)
                                        if docType.lastModified < lastModified {
                                            docType.lastModified = lastModified
                                        }
                                    }
                                    print("Total time spend on \(documentType) sync = \(NSDate().timeIntervalSince1970 - startTime) [s], no of documents = \((docGuid == nil) ? String(noOfDocuments) : docGuid!) started from = \(startFrom)")
                                }
                        }
                    }
                } else {
                    try! realm.write {
                        noOfDocuments = responseObj.count
                        if noOfDocuments > 0 {
                            if let documentType = responseObj.first?.1[PropertiesConsts.TYPE][PropertiesConsts.NAME].string {
                                if let docType = DocumentType.getDocumentType(docType: documentType, realm) {
                                    for (_, document):(String, JSON) in responseObj {

                                            if document[PropertiesConsts.GUID].exists() {
                                                docGuid = document[PropertiesConsts.GUID].stringValue
                                            }
                                            var lastModified = docType.lastModified
                                            propertyKeysList = getPropertiesForDocumentType(documentType: documentType, realm: realm)
                                            lastModified = singleDocument(lastModified: lastModified, propertyKeysList: propertyKeysList, documentJson: document, isAsset: isAsset, realm: realm)
                                            if docType.lastModified < lastModified {
                                                docType.lastModified = lastModified
                                            }
                                    }
                                }
                                print("Total time spend on \(documentType) sync = \(NSDate().timeIntervalSince1970 - startTime) [s], no of documents = \((docGuid == nil) ? String(noOfDocuments) : docGuid!) started from = \(startFrom)")
                            }
                        }
                    }
                }
            }
        } else if responseJSON[SyncConsts.DOCUMENTS].exists() {
            let responseObj = responseJSON[SyncConsts.DOCUMENTS]
            if responseObj.type == .array {
                noOfDocuments = responseObj.count
                if noOfDocuments > 0 {
                    if responseObj[0][PropertiesConsts.TYPE].exists() {
                        if isTransactionInProgress {
                            if let documentType = responseObj[0][PropertiesConsts.TYPE][PropertiesConsts.NAME].string {
                                if let docType = DocumentType.getDocumentType(docType: documentType, realm) {
                                    var lastModified = docType.lastModified
                                    let propertyKeysList = getPropertiesForDocumentType(documentType: documentType, realm: realm)
                                    
                                    for document in responseObj {
                                        lastModified = singleDocument(lastModified: lastModified, propertyKeysList: propertyKeysList, documentJson: document.1, isAsset: isAsset, realm: realm)
                                    }
                                    if docType.lastModified < lastModified {
                                        docType.lastModified = lastModified
                                    }
                                }
                                print("Total time spend on \(documentType) sync = \(NSDate().timeIntervalSince1970 - startTime) [s], no of documents = \(noOfDocuments) started from = \(startFrom)")
                            }
                        } else {
                            try! realm.write {
                                if let documentType = responseObj[0][PropertiesConsts.TYPE][PropertiesConsts.NAME].string {
                                    if let docType = DocumentType.getDocumentType(docType: documentType, realm) {
                                        var lastModified = docType.lastModified
                                        let propertyKeysList = getPropertiesForDocumentType(documentType: documentType, realm: realm)
                                        
                                        for document in responseObj {
                                            lastModified = singleDocument(lastModified: lastModified, propertyKeysList: propertyKeysList, documentJson: document.1, isAsset: isAsset, realm: realm)
                                        }
                                        if docType.lastModified < lastModified {
                                            docType.lastModified = lastModified
                                        }
                                    }
                                    print("Total time spend on \(documentType) sync = \(NSDate().timeIntervalSince1970 - startTime) [s], no of documents = \(noOfDocuments) started from = \(startFrom)")
                                }
                            }
                        }
                    }
                } else {
                    if docType != nil {
                        if isTransactionInProgress {
                            DocumentType.setLastModified(name: docType!, lastModified: serverLastModified, realm)
                        } else {
                            try! realm.write{
                                DocumentType.setLastModified(name: docType!, lastModified: serverLastModified, realm)
                            }
                        }
                    }
                }
            }
        }
        realm.refresh()
        return SyncResult(counterBatch: noOfDocuments, counterAll: countFromJson)!
    }
    
    func singleDocument(lastModified: Int64, propertyKeysList: [String: DocumentProperty], documentJson: JSON, isAsset: Bool, realm: Realm) -> Int64 {
        var lModified = lastModified
        if documentJson[SyncConsts.IS_DELETED].boolValue || documentJson[SyncConsts.IS_COMPLETED].boolValue {
            if !(documentJson[SyncConsts.LAST_MODIFIED].null != nil) {
                if lModified < documentJson[SyncConsts.LAST_MODIFIED].int64Value {
                    lModified = documentJson[SyncConsts.LAST_MODIFIED].int64Value
                }
            }
            
            if let guid = documentJson[SyncConsts.GUID].string {
                Document.deleteByGuid(guid: guid, realm)
            }
        } else {
            lModified = parseDoc(documentJson: documentJson, lastModified: lastModified, propertyKeysList: propertyKeysList, isAsset: isAsset, realm: realm)
        }
        return lModified
    }
    
    func getPropertiesForDocumentType(documentType : String, realm: Realm) -> [String: DocumentProperty] {
        var propertyKeysList = [String : DocumentProperty]()
        for documentProperty in DocumentProperty.getDocumentPropertiesByDocumentType(docType: documentType, realm) {
            propertyKeysList.updateValue(documentProperty, forKey: documentProperty.key!)
        }
        return propertyKeysList
    }
    
    func parseDoc(documentJson: JSON, lastModified: Int64, propertyKeysList: [String: DocumentProperty], isAsset: Bool, realm : Realm) -> Int64 {
        let guid = documentJson[SyncConsts.GUID].stringValue
        var isModified = false
        var document : Document? = nil
        
        var lModified = lastModified
        
        if isAsset {
            document = realm.create(Document.self, value: [Document.GUID : guid], update: false)
        } else {
            document = Document.getDocument(guid: guid, realm)
            if document == nil {
                document = realm.create(Document.self, value: [Document.GUID : guid], update: false)
            } else {
                isModified = true
            }
        }
        
        if documentJson[SyncConsts.TYPE].exists() {
            if let docType = DocumentType.getDocumentType(docType: documentJson[SyncConsts.TYPE][SyncConsts.NAME].stringValue, realm) {
                document?.type = docType
            }
            if documentJson[SyncConsts.LAST_MODIFIED].null == nil {
                document?.lastModified = documentJson[SyncConsts.LAST_MODIFIED].int64Value
            } else {
                document?.lastModified = 0
            }
            document?.created = documentJson[SyncConsts.CREATED].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
            document?.updated = documentJson[SyncConsts.UPDATED].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
            let lMod = (document?.lastModified)!
            
            if lModified < lMod {
                lModified = lMod
            }
            
            document?.availableActions = documentJson[SyncConsts.AVAILABLE_ACTIONS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
            
            if documentJson[SyncConsts.NAME].exists() {
                document?.listName = documentJson[SyncConsts.NAME].stringValue
            } else {
                document?.listName = "Name missing"
            }
            
            var documentPropertyValue : DocumentPropertyValue?
            
            for (_ ,var propertyValue) in documentJson[SyncConsts.PROPERTIES_VALUES]{
                let key = propertyValue[PropertiesConsts.KEY].stringValue
                if propertyKeysList.index(forKey: key) != nil {
                    
                    let property = propertyKeysList[key]!
                    let documentGuid = (document?.guid)!
                    
                    if isModified {
                        documentPropertyValue = DocumentPropertyValue.getDocumentPropertyValueByDpvId(dpvId: "\(property.id)_\(documentGuid)", realm)
                        if documentPropertyValue == nil {
                            documentPropertyValue = DocumentPropertyValue()
                            documentPropertyValue?.propertyId = (property.id)
                            documentPropertyValue?.dpvId = "\(property.id)_\(documentGuid)"
                        }
                    } else {
                        documentPropertyValue = DocumentPropertyValue()
                        documentPropertyValue?.propertyId = property.id
                        documentPropertyValue?.dpvId = "\(property.id)_\(documentGuid)"
                    }
                    
                    if propertyValue[PropertiesConsts.CONTENT].type == .dictionary {
                        let content = propertyValue[PropertiesConsts.CONTENT]
                        
                        if let classStr = property.classNameStr {
                            switch classStr {
                            case ClassTypeConsts.DOCUMENT:
                                if (content[PropertiesConsts.DOCUMENT_GUID].exists() && content[PropertiesConsts.DOCUMENT_GUID].null == nil) {
                                    var isSkipSync = false
                                    for docType in property.allowedDocumentTypes {
                                        if let dataFromString = docType.groups?.data(using: .utf8, allowLossyConversion: false) {
                                            do {
                                                let json = try JSON(data: dataFromString)
                                                let groupsArray:[String] = json.arrayValue.map { $0.string!}
                                                for asset in groupsArray {
                                                    if asset == SyncConsts.SKIP_SYNC || asset == SyncConsts.PREV_ASSIGNEES_SKIP_SYNC {
                                                        isSkipSync = true
                                                        break
                                                    }
                                                }
                                            } catch {
                                                print(error)
                                            }
                                        }
                                    }
                                    
                                    let docGuid = content[PropertiesConsts.DOCUMENT_GUID].stringValue
                                    if isSkipSync {
                                        if let doc = checkIfDocumentExists(documentId: docGuid, parentGuid: (document?.guid)!, propertyFrom: property.id, realm: realm) {
                                            documentPropertyValue?.documents.removeAll()
                                            documentPropertyValue?.documents.append(doc)
                                        }
                                    } else {
                                        var doc = Document.getDocument(guid: docGuid, realm)
                                        if doc == nil {
                                            doc = createNewDocumentWithGuid(docGuid: docGuid)
                                        }
                                        documentPropertyValue?.documents.removeAll()
                                        documentPropertyValue?.documents.append(doc!)
                                    }
                                }
                            case ClassTypeConsts.DOCUMENTLIST,
                                 ClassTypeConsts.DOCUMENTSINGLELIST:
                                if (content[PropertiesConsts.DOCUMENT_GUIDS].exists() && content[PropertiesConsts.DOCUMENT_GUIDS].null == nil) {
                                    var isSkipSync = false
                                    for docType in property.allowedDocumentTypes {
                                        if let dataFromString = docType.groups?.data(using: .utf8, allowLossyConversion: false) {
                                            do {
                                                let json = try JSON(data: dataFromString)
                                                let groupsArray:[String] = json.arrayValue.map { $0.string!}
                                                for asset in groupsArray {
                                                    if asset == SyncConsts.SKIP_SYNC || asset == SyncConsts.PREV_ASSIGNEES_SKIP_SYNC {
                                                        isSkipSync = true
                                                        break
                                                    }
                                                }
                                            } catch {
                                                print(error)
                                            }
                                        }
                                    }
                                    
                                    let docGuids = content[PropertiesConsts.DOCUMENT_GUIDS].arrayValue.map { $0.string!}
                                    if docGuids.count > 0 {
                                        if isSkipSync {
                                            for docGuid in docGuids {
                                                if let doc = checkIfDocumentExists(documentId: docGuid, parentGuid: (document?.guid)!, propertyFrom: property.id, realm: realm) {
                                                    if !(documentPropertyValue?.documents.contains(doc))! {
                                                        if classStr == ClassTypeConsts.DOCUMENTSINGLELIST {
                                                            documentPropertyValue?.documents.removeAll()
                                                        }
                                                        documentPropertyValue?.documents.append(doc)
                                                    }
                                                }
                                            }
                                        } else {
                                            for docGuid in docGuids {
                                                var doc = Document.getDocument(guid: docGuid, realm)
                                                if doc == nil {
                                                    doc = createNewDocumentWithGuid(docGuid: docGuid)
                                                }
                                                if !(documentPropertyValue?.documents.contains(doc!))! {
                                                    if classStr == ClassTypeConsts.DOCUMENTSINGLELIST {
                                                        documentPropertyValue?.documents.removeAll()
                                                    }
                                                    documentPropertyValue?.documents.append(doc!)
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            case ClassTypeConsts.PRIMITIVE,
                                 ClassTypeConsts.ENTITY,
                                 ClassTypeConsts.KEY,
                                 ClassTypeConsts.ENUMERATE,
                                 ClassTypeConsts.ENUMERATE_MULTIPLE,
                                 ClassTypeConsts.BOOLEAN,
                                 ClassTypeConsts.POLYLINE,
                                 ClassTypeConsts.POLYGON,
                                 ClassTypeConsts.SCHEDULE_AVAILABILITY,
                                 ClassTypeConsts.DOCUMENTWORKFLOWSTATE:
                                if !(content[PropertiesConsts.BODY].null != nil) {
                                    //save simple value
                                    documentPropertyValue?.content = content[PropertiesConsts.BODY].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                                } else {
                                    //Temp
                                    if (classStr == ClassTypeConsts.PRIMITIVE) {
                                        documentPropertyValue?.content = "Name missing"
                                    }
                                }
                            case ClassTypeConsts.BINARY:
                                if content[PropertiesConsts.BODY].null == nil {
                                    documentPropertyValue?.content = content[PropertiesConsts.BODY].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                                    
                                }
                                //saving MIME info to additionalContent
                                if content[PropertiesConsts.MIME].null == nil {
                                    documentPropertyValue?.additionalContent = content[PropertiesConsts.MIME].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                                }
                            case ClassTypeConsts.DATE,
                                 ClassTypeConsts.DATETIME:
                                //dates
                                if content[PropertiesConsts.BODY].null == nil {
                                    //save value in date format
                                    documentPropertyValue?.content = content[PropertiesConsts.BODY].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                                    if !(documentPropertyValue?.content == "") {
                                        let tz = TimeZoneCustom.getTimezone(json: (documentPropertyValue?.content)!)
                                        if (tz.date != nil && tz.format != nil) {
                                            if (!(tz.date == "") && !(tz.format == "")) {
                                                if (tz.format?.contains("Y"))! {
                                                    tz.format = tz.format?.replacingOccurrences(of: "Y", with: "yyyy")
                                                }
                                                if (tz.format?.contains("m"))! {
                                                    tz.format = tz.format?.replacingOccurrences(of: "m", with: "MM")
                                                }
                                                if (tz.format?.contains("d"))! {
                                                    tz.format = tz.format?.replacingOccurrences(of: "d", with: "dd")
                                                }
                                                if (tz.format?.contains("i"))! {
                                                    tz.format = tz.format?.replacingOccurrences(of: "i", with: "mm")
                                                }
                                                if (tz.format?.contains("H"))! {
                                                    tz.format = tz.format?.replacingOccurrences(of: "H", with: "HH")
                                                }
                                                let formatter = DateFormatter()
                                                formatter.dateFormat = tz.format
                                                formatter.timeZone = NSTimeZone.default
                                                
                                                if let settings = UserDefaults.standard.dictionary(forKey: PropertiesConsts.SETTINGS) {
                                                    if let stringOne = settings[DocTypeConsts.OFFICE_SETTINGS_TIMEZONE] as? String {
                                                        formatter.timeZone = NSTimeZone(name: stringOne)! as TimeZone
                                                    }
                                                }
                                                
                                                var dateJFull = tz.date!.split{$0 == "."}.map(String.init)
                                                if let date = formatter.date(from: dateJFull[0]) {
                                                    documentPropertyValue?.contentDate = date
                                                }
                                            }
                                        }
                                    }
                                }
                            case ClassTypeConsts.DOCUMENTCREATED,
                                 ClassTypeConsts.DOCUMENTUPDATED,
                                 ClassTypeConsts.PROBLEM_SLA:
                                //dates
                                if content[PropertiesConsts.BODY].null == nil {
                                    //save value in date format
                                    let body = content[PropertiesConsts.BODY]
                                    documentPropertyValue?.content = body.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                                    if !(documentPropertyValue?.content == "") {
                                        let dateJson = content[PropertiesConsts.BODY]
                                        if dateJson[PropertiesConsts.DATE].exists() {
                                            let dateJ = dateJson[PropertiesConsts.DATE].stringValue
                                            if dateJson[PropertiesConsts.TIMEZONE].exists() {
                                                var timezone = dateJson[PropertiesConsts.TIMEZONE].stringValue
                                                timezone = timezone.replacingOccurrences(of: ":", with: "")
                                                
                                                let formatter = DateFormatter()
                                                formatter.dateFormat = Constants.COMPUTED_DATETIME_FORMAT
                                                formatter.timeZone = NSTimeZone.default
                                                
                                                var dateJFull = dateJ.split{$0 == "."}.map(String.init)
                                                if let date = formatter.date(from: dateJFull[0]) {
                                                    documentPropertyValue?.contentDate = date
                                                }
                                            }
                                        }
                                    }
                                }
                            case ClassTypeConsts.LOCATION,
                                 ClassTypeConsts.RELATED_PROBLEM_LOCATION:
                                //location data
                                
                                if content[PropertiesConsts.BODY].null == nil {
                                    //save location
                                    let locationArray = content[PropertiesConsts.BODY].arrayValue
                                    documentPropertyValue?.content = "[\(locationArray[0]),\(locationArray[1])]"
                                }
                            case ClassTypeConsts.RELATED_PROBLEM:
                                //worker
                                if content[PropertiesConsts.BODY].null == nil {
                                    let body = content[PropertiesConsts.BODY]
                                    if body[PropertiesConsts.DOCUMENT_GUID].exists() {
                                        let docGuid = body[PropertiesConsts.DOCUMENT_GUID].stringValue
                                        if let doc = checkIfDocumentExists(documentId: docGuid, parentGuid: document!.guid!, propertyFrom: property.id, realm: realm) {
                                            documentPropertyValue?.documents.removeAll()
                                            documentPropertyValue?.documents.append(doc)
                                        }
                                    }
                                }
                            case ClassTypeConsts.TASK_ASSIGNEE:
                                //worker
                                if content[PropertiesConsts.BODY].null == nil {
                                    let body = content[PropertiesConsts.BODY]
                                    if body[PropertiesConsts.GUID].exists() {
                                        if let doc = checkIfDocumentExists(documentId: body[PropertiesConsts.GUID].stringValue, parentGuid: document!.guid!, propertyFrom: property.id, realm: realm) {
                                            documentPropertyValue!.documents.removeAll()
                                            documentPropertyValue!.documents.append(doc)
                                        }
                                    }
                                } else {
                                    documentPropertyValue?.documents.removeAll()
                                }
                            case ClassTypeConsts.DOCUMENTCREATOR,
                                 ClassTypeConsts.USER:
                                //system user
                                if content[PropertiesConsts.BODY].null == nil {
                                    let user = content[PropertiesConsts.BODY]
                                    if user.null == nil {
                                        if user[PropertiesConsts.DISPLAY_NAME].exists() {
                                            let displayName = user[PropertiesConsts.DISPLAY_NAME].stringValue
                                            documentPropertyValue?.content = displayName
                                        }
                                        if user[PropertiesConsts.ID].exists() {
                                            let id = user[PropertiesConsts.ID].stringValue
                                            documentPropertyValue?.additionalContent = id
                                        }
                                    }
                                } else {
                                    documentPropertyValue?.content = nil
                                    documentPropertyValue?.additionalContent = nil
                                }
                            default:
                                let content = content[PropertiesConsts.BODY].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                                documentPropertyValue?.content = (content == "null") ? nil : content
                            }
                        }
                    }
                }
                document!.propertyValues.append(documentPropertyValue!)
            }
        }
        return lModified
    }
    
    func createNewDocumentWithGuid(docGuid: String) -> Document {
        let document = Document.newDocument()
        document.guid = docGuid
        return document
    }

    func checkIfDocumentExists(documentId: String, parentGuid: String, propertyFrom: Int, realm: Realm) -> Document? {
        let startTime = Date().timeIntervalSince1970
        let docStatus = Document.checkIfAddDocument2Queue(documentGuid: documentId, realm)
        realm.refresh()
        switch (docStatus) {
        case 0:
            print("Document = \(documentId) already exists")
            return Document.getDocument(guid: documentId, realm)!
        case 1:
            return nil
        case 2:
//            var params = Parameters()
//            params[SyncConsts.ID] = documentId
//
//            let out = Queue.newQueue()
//            var index = 0
//
//            let num = realm.objects(Queue.self).max(ofProperty: "id") as Int?
//
//            if num != nil {
//                index = num!
//            }
//            index = index + 1
//
//            out.id = index
//            out.method = ServerHelper.GET
//            out.params = RequestObject.dict2nsData(params)
//            out.endpoint = Config.API.Operations.document.endpoint
//            out.executeImmediately = false
//            out.documentGuid = documentId
//            out.sendFromDocumentProperty = propertyFrom
//
//            realm.add(out)
            
            let document = createNewDocumentWithGuid(docGuid: documentId)
            
            print("Time spend on adding new document id = \(documentId), for parentGuid = \(parentGuid), propertyFrom = \(propertyFrom), to queue = \(Date().timeIntervalSince1970 - startTime) [s]")
            return document
        default:
            return nil
        }
    }
    
//    static func startSync(type: String, startFrom: Int, maxUpdateTS: Int, completion2: @escaping (_ counter: Int) -> ()) {
//        var params = Parameters()
//        params[SyncConsts.TYPE] = type
//        params[SyncConsts.LAST_MODIFIED] = String(maxUpdateTS)
//        params[SyncConsts.MAX_RESULTS] = String(noOfResults)
//        params[SyncConsts.START_FROM] = String(startFrom)
//        switch(type) {
//        case DocTypeConsts.MC3_ENQUIRY:
//            params = syncFilter(params: params, key: PropertiesConsts.REPORTER, value: UserDefaults.standard.string(forKey: PropertiesConsts.REPORTER_GUID)!)
//            break
//        case DocTypeConsts.MC3_ENQUIRY_SOURCE:
//            params = syncFilter(params: params, key: PropertiesConsts.KEY, value: PropertiesConsts.MC3_ENQUIRY_SOURCE_ZAPPAPP)
//            break
//        case DocTypeConsts.MC3_ENQUIRY_SLA:
//            params = syncFilter(params: params, key: PropertiesConsts.KEY, value: PropertiesConsts.MC3_ENQUIRY_SLA_10_DAYS_STANDARD)
//            break
//        case DocTypeConsts.MC3_ENQUIRY_TYPE:
//            params = syncFilter(params: params, key: PropertiesConsts.SEEN_IN_ZAPP_APP, value: "1")
//            break
//        default:
//            break
//        }
//
//        let updateProfileThread = DispatchQueue(label: "xx", qos: .background, target: nil)
//        updateProfileThread.async {
//            ApiDocument().getList(params: params, endPoint: Config.API.Operations.document.endpoint, queue: updateProfileThread, completion:{ (finished: Bool, response: JSON?) -> Void in
//                if finished{
//                    completion2(parseDocument(response!, onlyInsert: false))
//                }
//            })
//        }
//    }
}
