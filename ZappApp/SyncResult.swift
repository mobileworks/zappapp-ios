//
//  SyncResult.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit

class SyncResult {
    
    // MARK: Properties
    var counterBatch: Int
    var counterAll: Int
    
    // MARK: Initialization
    
    init?(counterBatch: Int, counterAll: Int) {
        // Initialize stored properties.
        self.counterBatch = counterBatch
        self.counterAll = counterAll
    }
}
