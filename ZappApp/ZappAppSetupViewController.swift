//
//  ZappAppSetupViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVFoundation

class ZappAppSetupViewController: UIViewController, UITextFieldDelegate {
    @available(iOS 2.0, *)
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //MARK: Properties
    
    @IBOutlet weak var instanceUrlTextField: UITextField!
    @IBOutlet weak var instanceUrlLabel: UILabel!
    @IBOutlet weak var urlSwitch: UISwitch!
    
    var pickerData: [String] = [String]()
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setupForView(self.view)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        var instance = UserDefaults.standard.string(forKey: PropertiesConsts.INSTANCE)
        
        instanceUrlTextField.delegate = self
        instanceUrlTextField.returnKeyType = .done
        instanceUrlTextField.autocorrectionType = .no
        
        if instance != nil {
            if !(instance!.hasPrefix("http")) {
                instance = "http://\(instance!)"
            }
            appDelegate.savedInstance = instance!
            DispatchQueue.main.async(execute: {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                self.present(storyboard.instantiateViewController(withIdentifier: "LoginRegister"), animated: true, completion: nil)
            })
        }
        
        navigationItem.title = "setup".localized
        
        let qrItem = UIBarButtonItem.init(image: UIImage(named: "QrCode"), style: .plain, target: self, action: #selector(qrcodeScanner))
        navigationItem.setRightBarButton(qrItem, animated: true)
    }
    
    @objc func qrcodeScanner() {
        checkCamera()
    }
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch authStatus {
        case .authorized: present(ScannerViewController(self), animated:true, completion:nil)
        case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(title: "", message: "cameraAccessRequired".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "allowCamera".localized, style: .cancel, handler: { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) -> Void in
            DispatchQueue.main.async() {
                self.checkCamera()
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    //MARK: Actions
    
    @IBAction func switchFullURL(_ sender: UISwitch) {
        if sender.isOn {
            let alertController = UIAlertController(title: "doYouWantToEnterFullInstance".localized, message: "", preferredStyle: UIAlertController.Style.alert)
            
            let saveAction = UIAlertAction(title: "yes".localized, style: .default, handler: {
                alert in
                self.instanceUrlLabel.isHidden = true
            })
            
            let cancelAction = UIAlertAction(title: "no".localized, style: .default, handler: {
                (action : UIAlertAction!) -> Void in
                sender.isOn = false
            })
            
            alertController.addAction(cancelAction)
            alertController.addAction(saveAction)
            
            present(alertController, animated:true, completion:nil)
        } else {
            instanceUrlLabel.isHidden = false
        }
    }
    
    func extractedFunc(_ url: String) {
        DispatchQueue.main.async(execute: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.present(storyboard.instantiateViewController(withIdentifier: "LoginRegister"), animated: true, completion: nil)
        })
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.savedInstance = url
        UserDefaults.standard.setValue(url, forKey: PropertiesConsts.INSTANCE)
        UserDefaults.standard.synchronize()
        self.activityIndicator.stop()
    }
    
    @IBAction func saveInstanceUrlAction(_ sender: UIButton) {
        activityIndicator.start()
        
        if Reachability.isConnectedToNetwork() {
            if var url = instanceUrlTextField.text {
                if url == "" {
                    AlertMessage.displayAlertMessage(self, title: "urlMissing".localized, message: "youMustProvideInstanceAddress".localized, actionLabel: "ok".localized)
                    activityIndicator.stop()
                } else {
                    if !instanceUrlLabel.isHidden {
                        url = "\(url)\(instanceUrlLabel.text!)"
                    }
                    if (!(url.hasPrefix("http"))) {
                        url = "https://\(url)"
                    }

                    Alamofire.request(url).responseString { response in
                        let statusCode = response.response?.statusCode
                        if statusCode == 200 {
                            self.extractedFunc(url)
                        } else if statusCode == 301 {
                            if url.hasPrefix("http:") {
                                url = url.replacingOccurrences(of: "http:", with: "https:")
                                self.extractedFunc(url)
                            }
                        } else if statusCode == 302 {
                            if url.hasPrefix("https:") {
                                url = url.replacingOccurrences(of: "https:", with: "http:")
                                self.extractedFunc(url)
                            }
                        } else {
                            AlertMessage.displayAlertMessage(self, title: "wrongUrl".localized, message: "youMustProvideCorrectInstanceAddress".localized, actionLabel: "ok".localized)
                            self.activityIndicator.stop()
                        }
                    }
                }
            }
        } else {
            AlertMessage.displayAlertMessage(self, title: "noInternetConnection".localized, message: "pleaseCheckYourInternetConnection".localized, actionLabel: "ok".localized)
            activityIndicator.stop()
        }
    }
}
