//
//  AppDelegate.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 21.02.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var enquiryPhotos: [UIImage] = []
    var enquiry : NewEnquiry = NewEnquiry()
    var savedInstance = ""
    
    var status = false
    var showedLocationPermision = false
    var loggedIn = false

    private func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        // Set this as the configuration used for the default Realm
        var config = Realm.Configuration()
        
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("default.realm")
        Realm.Configuration.defaultConfiguration = config
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {        
        if showedLocationPermision && !loggedIn {
            showedLocationPermision = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Test") as UIViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

