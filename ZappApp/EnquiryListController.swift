//
//  EnquiryListController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 19.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class EnquiryListController: UITableViewController {
    
    //MARK: Properties
    var enquiries = [Enquiry]()
    
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setupForView(self.view)
        loadEnquiriesForUser()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.loggedIn = true
    }
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enquiries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "EnquiryListCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath) as! EnquiryListCell
        
        let enquiry = enquiries[indexPath.row]
        
        cell.address.text = enquiry.enquiryAddress
        cell.enquiryType.text = enquiry.enquiryTypeName
        cell.created.text = enquiry.created
        cell.enquiryStatusImage.image = enquiry.enquiryStatusPhoto
        return cell
    }
    
    func loadEnquiriesForUser() {
        enquiries.removeAll()
        let realm = try! Realm()
        
        if let documents = Document.getDocumentsByTypeFilterByPropertyKeyDocGuid(type: DocTypeConsts.MC3_ENQUIRY, propertyKey: PropertiesConsts.REPORTER, docGuid: UserDefaults.standard.string(forKey: PropertiesConsts.REPORTER_GUID)!, realm) {
        
            for document in documents {
                var address: String?
                var enquiryType: String?
                var created: String?
                var statusPhoto: UIImage = UIImage()
                
                let properties = DocumentProperty.getDocumentPropertiesByDocumentType(docType: DocTypeConsts.MC3_ENQUIRY, realm)
                
                for property in properties {
                    switch property.key! {
                    case PropertiesConsts.ADDRESS:
                        if let add = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                            address = add.content
                        }
                    case PropertiesConsts.CREATED:
                        if let createdStr = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                            if let date = createdStr.contentDate {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                                let strDate = dateFormatter.string(from: date)
                                created = "Created: \(strDate)"
                            }
                        }
                    case PropertiesConsts.STATUS:
                        if let enquiryStatusGuid = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                            if !enquiryStatusGuid.documents.isEmpty {
                                if let enquiryStatusDoc = enquiryStatusGuid.documents.first {
                                    if enquiryStatusDoc.guid != nil {
                                        if let status = DocumentParser.getContentFromDp(docType: DocTypeConsts.MC3_ENQUIRY_STATUS, property: PropertiesConsts.KEY, docGuid: enquiryStatusDoc.guid!, realm) {
                                            switch status {
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_NEW:
                                                    statusPhoto = UIImage(named: "New")!
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_OPEN:
                                                    statusPhoto = UIImage(named: "Open")!
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_CLOSED:
                                                    statusPhoto = UIImage(named: "Resolved")!
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_REASSIGN:
                                                    statusPhoto = UIImage(named: "Reassign")!
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_ATTENDING_TO_SOLVE_THE_ISSUE:
                                                    statusPhoto = UIImage(named: "Attending to solve")!
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_ASSIGNED:
                                                    statusPhoto = UIImage(named: "Assigned")!
                                                case PropertiesConsts.MC3_ENQUIRY_STATUS_DELETED:
                                                    statusPhoto = UIImage(named: "Deleted")!
                                                default:
                                                    statusPhoto = UIImage(named: "New")!
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    case PropertiesConsts.ENQUIRY_TYPE:
                        if let enquiry = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                            if !enquiry.documents.isEmpty {
                                if let enquiryTypeDoc = enquiry.documents.first {
                                    if enquiryTypeDoc.guid != nil {
                                        enquiryType = DocumentParser.getContentFromDp(docType: DocTypeConsts.MC3_ENQUIRY_TYPE, property: PropertiesConsts.NAME, docGuid: enquiryTypeDoc.guid!, realm)
                                    }
                                }
                            }
                        }
                    default:
                        break
                    }
                }
                
                if let en = Enquiry(enquiryAddress: address, enquiryTypeName: enquiryType, created: created, enquiryStatusPhoto: statusPhoto) {
                    enquiries.append(en)
                }
            }
        }
    }
    
    @IBAction func resync(_ sender: UIBarButtonItem) {
        activityIndicator.start()
        GetDataThread().run(completion: {status in
            if status {
                self.loadEnquiriesForUser()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.activityIndicator.stop()
                }
            }
        })
    }
}
