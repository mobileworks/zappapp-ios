//
//  LoginView.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 28.02.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class LoginView: UIViewController {

    //MARK: PROPERTIES
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var PINField: UITextField!
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    
    let DEFAULT_USER = "guest"
    let DEFAULT_PASSWORD = "JustADummyPassword"
    
    var reporterGuid : String = ""
    var serverPIN : String = ""
    var userPIN :String = ""
    var userMail :String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setupForView(self.view)
        emailField.text = UserDefaults.standard.string(forKey: PropertiesConsts.USER_EMAIL)
        if (emailField.text != nil) {
            userMail = emailField.text!
        }
        PINField.text = UserDefaults.standard.string(forKey: PropertiesConsts.USER_PIN)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginAction(_ sender: UIButton) {
        userPIN = PINField.text!
        userMail = emailField.text!
        var cancel = false
        var cancelLabel = ""
        
        if userPIN.isEmpty {
            PINField.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "PIN is empty, \n"
        } else {
            if userPIN.count < 4 {
                PINField.layer.borderColor = UIColor.red.cgColor
                cancel = true
                cancelLabel += "PIN is too short, \n"
            }
            PINField.layer.borderColor = UIColor.clear.cgColor
        }
        
        // Check for a valid user email.
        if userMail.isEmpty {
            emailField.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "email is empty, \n"
        } else {
            if (!(EmailValidator.isValidEmail((emailField.text)!))) {
                emailField.layer.borderColor = UIColor.red.cgColor
                cancel = true
                cancelLabel += "email is in wrong format, \n"
            }
        }
        if cancel {
            AlertMessage.displayAlertMessage(self, title: "fieldsWronglyFilled".localized, message: cancelLabel, actionLabel: "ok".localized)
        } else {
            let parameters: Parameters = [
                "username": DEFAULT_USER,
                "password": DEFAULT_PASSWORD
            ]
            
            emailField.resignFirstResponder()
            PINField.resignFirstResponder()
            
            activityIndicator.start()
            
            let loginThread = DispatchQueue(label: "login", qos: .background, target: nil)
            loginThread.async {
                ApiLogin().call(params: parameters, queue: loginThread, completion:{ (finished: Bool, response: JSON?) ->  Void in
                    if finished{
                        if response![SyncConsts.SUCCESS].boolValue {
                            
                            RegisterView.getJSONFromModule(response: response!)
                            CurrentUser().storeUser(userParams: response!["user"])

                            self.checkIfReporterExists(userMail: self.userMail, userPIN: self.userPIN, completion: {status in
                                switch(status) {
                                case 3:
                                    DispatchQueue.main.async(execute: {
                                        self.activityIndicator.stop()
                                        AlertMessage.displayAlertMessage(self, title: "wrongPin".localized, message: "errorWhenCheckingPin".localized, actionLabel: "ok".localized)
                                    })
                                case 2:
                                    UserDefaults.standard.set(self.userMail, forKey: PropertiesConsts.USER_EMAIL)
                                    UserDefaults.standard.set(self.userPIN, forKey: PropertiesConsts.USER_PIN)
                                    UserDefaults.standard.set(self.reporterGuid, forKey: PropertiesConsts.REPORTER_GUID)
                                    UserDefaults.standard.synchronize()
                                    
                                    GetDataThread().run(completion: {status in
                                        if status {
                                            DispatchQueue.main.async(execute: {
                                                self.activityIndicator.stop()
                                                self.present(TabBarMainController(), animated: true, completion: nil)
                                            })
                                        } else {
                                            DispatchQueue.main.async(execute: {
                                                self.activityIndicator.stop()
                                                AlertMessage.displayAlertMessage(self, title: "errorWhenDownloadingData".localized, message: "errorWhenCheckingUser", actionLabel: "ok".localized)
                                            })
                                        }
                                    })
                                case 1:
                                    DispatchQueue.main.async(execute: {
                                        self.activityIndicator.stop()
                                        AlertMessage.displayAlertMessage(self, title: "emailDoesNotExist".localized, message: "errorWhenCheckingEmail".localized, actionLabel: "ok".localized)
                                    })
                                case 0:
                                    DispatchQueue.main.async(execute: {
                                        self.activityIndicator.stop()
                                        AlertMessage.displayAlertMessage(self, title: "registerError".localized, message: "errorWhenCheckingUser".localized, actionLabel: "ok".localized)
                                    })
                                default:
                                    break
                                }
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.activityIndicator.stop()
                                AlertMessage.displayAlertMessage(self, title: "authenticationFailed".localized, message: "pleaseTryAgain".localized, actionLabel: "ok".localized)
                            })
                        }
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stop()
                            AlertMessage.displayAlertMessage(self, title: "connectionError".localized, message: "pleaseTryAgain".localized, actionLabel: "ok".localized)
                        })
                    }
                })
            }
        }
    }
    
    @IBAction func forgetPinButton(_ sender: UIButton) {
        userMail = emailField.text!
        promptForAnswer(userMail)
    }
    
    func checkIfReporterExists(userMail : String, userPIN: String, completion:@escaping (_ status : Int) -> ()) {
        
        var params = Parameters()
        params[SyncConsts.TYPE] = DocTypeConsts.CRM_CONTACT
        params[SyncConsts.LAST_MODIFIED] = "0"
        params[SyncConsts.MAX_RESULTS] = "10"
        params[SyncConsts.PROPERTIES] = JSON(Parameters()).rawString()
        
        let json : JSON = [PropertiesConsts.EMAIL: userMail]
        params[SyncConsts.FILTERS] = json.rawString()
        
        let checkReporterThread = DispatchQueue(label: "reporter", qos: .background, target: nil)
        checkReporterThread.async {
        
            ApiDocument().getList(params: params, endPoint: Config.API.Operations.document.endpoint, queue: checkReporterThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished {
                    let realm = try! Realm()
                    for (key, doc):(String, JSON) in response! {
                        if(key == SyncConsts.DOCUMENTS) {
                            let array = doc.arrayValue
                            if array.count == 0 {
                                completion(1)
                            } else {
                                SharedSync().parseDocument(responseJSON: response!, realm: realm)
                                let jsonObject = array[0] as JSON
                                if jsonObject != JSON.null {
                                    let repGuid = jsonObject[SyncConsts.GUID]
                                    self.reporterGuid = repGuid.stringValue
                                    
                                    let propertiesValues = jsonObject[SyncConsts.PROPERTIES_VALUES].arrayValue
                                    for(key) in propertiesValues {
                                        let pKey =  key[PropertiesConsts.KEY].stringValue
                                        if pKey == PropertiesConsts.PIN_CODE {
                                            self.serverPIN = key[PropertiesConsts.CONTENT][PropertiesConsts.BODY].stringValue
                                            if userPIN == self.serverPIN {
                                                for values in DocumentPropertyValue.getDocumentPropertyValuesByDocumentGuid(documentGuid: self.reporterGuid, realm) {
                                                    if let property = DocumentProperty.getDocumentPropertyById(id: values.propertyId, realm) {
                                                        let key = property.key
                                                        if (key == PropertiesConsts.TITLE) {
                                                            UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_TITLE)
                                                        } else if (key == PropertiesConsts.FIRST_NAME) {
                                                            UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_FIRST_NAME)
                                                        } else if (key == PropertiesConsts.LAST_NAME) {
                                                            UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_LAST_NAME)
                                                        } else if (key == PropertiesConsts.PHONE_NUMBER) {
                                                            UserDefaults.standard.set(values.content, forKey: PropertiesConsts.USER_PHONE_NO)
                                                        }
                                                        UserDefaults.standard.synchronize()
                                                    }
                                                }
                                                completion(2)
                                            } else {
                                                completion(3)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    completion(0)
                }
            })
        }
    }
    
    func promptForAnswer(_ email: String) {
        let ac = UIAlertController(title: "enterEmail".localized, message: nil, preferredStyle: .alert)
        ac.addTextField(configurationHandler: nil)
        
        let tv = ac.textFields![0]
        tv.text = email
        
        let submitAction = UIAlertAction(title: "sendPinToEmail".localized, style: .default) { [unowned self, ac] (action: UIAlertAction!) in
            let answer = ac.textFields![0]
            
            if let emailTxt = answer.text {
                self.remindPIN(emailTxt)
            }
        }
        
        ac.addAction(submitAction)
        
        let cancelAction = UIAlertAction(title: "cancel".localized, style: .default, handler: {
            (alertAction:UIAlertAction!) in ac.dismiss(animated: true, completion: nil)
        })
        ac.addAction(cancelAction)
        
        present(ac, animated: true, completion: nil)
    }
    
    func remindPIN(_ email: String) {
        let parameters: Parameters = [
            "username": DEFAULT_USER,
            "password": DEFAULT_PASSWORD
        ]
        
        userPIN = PINField.text!;
        userMail = emailField.text!;
        
        emailField.resignFirstResponder()
        PINField.resignFirstResponder()
        
        activityIndicator.start()
        let loginThread = DispatchQueue(label: "login", qos: .background, target: nil)
        loginThread.async {
            ApiLogin().call(params: parameters, queue: loginThread, completion:{ (finished: Bool, response: JSON?) ->Void in
                if finished{
                    if (response?[SyncConsts.SUCCESS].boolValue)! {
                        
                        RegisterView.getJSONFromModule(response: response!)
                        CurrentUser().storeUser(userParams: response!["user"])

                        self.checkIfReporterExists(userMail: self.userMail, userPIN: "", completion: {status in
                            switch(status) {
                            case 3:
                                if self.reporterGuid != "" {
                                    self.sendPINAction(self.reporterGuid, completion: { (status) in
                                        if status {
                                            DispatchQueue.main.async(execute: {
                                                self.activityIndicator.stop()
                                                AlertMessage.displayAlertMessage(self, title: "pinRequestWasSent".localized, message: "waitForEmailToArrive".localized, actionLabel: "ok".localized)
                                            })
                                        } else {
                                            DispatchQueue.main.async(execute: {
                                                self.activityIndicator.stop()
                                                AlertMessage.displayAlertMessage(self, title: "pinRequestError".localized, message: "errorWhenSendingPinRequest".localized, actionLabel: "ok".localized)
                                            })
                                        }
                                    })
                                }
                                
                            case 1:
                                DispatchQueue.main.async(execute: {
                                    self.activityIndicator.stop()
                                    AlertMessage.displayAlertMessage(self, title: "emailDoesNotExist".localized, message: "thereIsNoUserContactForThisEmail".localized, actionLabel: "ok".localized)
                                })
                            default:
                                break
                            }
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stop()
                            AlertMessage.displayAlertMessage(self, title: "authenticationFailed".localized, message: "pleaseTryAgain".localized, actionLabel: "ok".localized)
                        })
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stop()
                        AlertMessage.displayAlertMessage(self, title: "connectionError".localized, message: "pleaseTryAgain".localized, actionLabel: "ok".localized)
                    })
                }
            })
        }
    }
    
    func sendPINAction(_ reporterGuid: String, completion:@escaping (_ status : Bool) -> ()) {
        
        var params = Parameters()
        params[SyncConsts.ACTION] = "assignPINCode"
        params[SyncConsts.PROPERTIES] = [AnyObject]()
        
        let pinThread = DispatchQueue(label: "pin", qos: .background, target: nil)
        pinThread.async {
            ApiDocument().update(content: params, endPoint: Config.API.Operations.document.endpoint+"/"+reporterGuid, senderParam: 0, queue: pinThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished{
                    for (key, doc):(String, JSON) in response! {
                        if key == SyncConsts.DOCUMENT {
                            let guid = doc[SyncConsts.GUID].string
                            if guid != nil {
                                completion(true)
                            }
                        } else {
                            completion(false)
                        }
                    }
                } else {
                    completion(false)
                }
            })
        }
    }
}
