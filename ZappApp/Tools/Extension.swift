//
//  Extension.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import ObjectiveC
import UIKit

var TagStrObjectHandle: UInt8 = 7
var ActionViewObjectHandle: UInt8 = 9

extension UIView {
    var tagStr: String? {
        get {
            return objc_getAssociatedObject(self, &TagStrObjectHandle) as? String ?? ""
        }
        set {
            objc_setAssociatedObject(self, &TagStrObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!
    }

    var actionView: UIViewController? {
        get {
            return objc_getAssociatedObject(self, &ActionViewObjectHandle) as? UIViewController
        }
        set {
            objc_setAssociatedObject(self, &ActionViewObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    public func base64(_ format: ImageFormat) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = self.pngData()
        case .jpeg(let compression): imageData = self.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
}

extension UITabBarController {
    func setTabBarVisible(_ visible: Bool) {
        setTabBarVisible(visible, duration: 0.3, animated: true)
    }
    
    func setTabBarVisible(_ visible:Bool, duration: TimeInterval, animated:Bool) {
        if (tabBarIsVisible() == visible) { return }
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        UIViewPropertyAnimator(duration: duration, curve: .linear) {
            self.tabBar.frame.offsetBy(dx:0, dy:offsetY)
            self.view.frame = CGRect(x:0,y:0,width: self.view.frame.width, height: self.view.frame.height + offsetY)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
            }.startAnimation()
    }
    
    func tabBarIsVisible() ->Bool {
        return self.tabBar.frame.origin.y < UIScreen.main.bounds.height
    }
}
