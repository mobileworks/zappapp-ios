//
//  DocumentParser.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 21.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//



import Foundation
import UIKit
import SwiftyJSON
import RealmSwift
import CoreLocation

class DocumentParser {
    
    static func getLocation(_ json: String) -> CLLocation? {
        var latLngStr: [String]? = nil
        if json.count > 0 {
            let start = json.index(json.startIndex, offsetBy: 1)
            let end = json.index(json.endIndex, offsetBy: -1)
            let range = start..<end
            let jsonT = json[range]
            latLngStr = jsonT.components(separatedBy: ",")
            if latLngStr?.count == 2 {
                return CLLocation(latitude: Double(latLngStr![0])!, longitude: Double(latLngStr![1])!)
            }
        }
        return nil
    }
    
    static func getLocationCordinate(_ json: String) -> CLLocationCoordinate2D? {
        var latLngStr: [String]? = nil
        if json.count > 0 {
            let start = json.index(json.startIndex, offsetBy: 1)
            let end = json.index(json.endIndex, offsetBy: -1)
            let range = start..<end
            let jsonT = json[range]
            latLngStr = jsonT.components(separatedBy: ",")
            if latLngStr?.count == 2 {
                return CLLocationCoordinate2D(latitude: Double(latLngStr![0])!, longitude: Double(latLngStr![1])!)
            }
        }
        return nil
    }
    
    static func getContentFromDp(docType: String, property: String, docGuid: String, _ realm: Realm) -> String? {
        if let documentProperty = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: docType, key: property, realm) {
            if let documentPropertyValue = DocumentPropertyValue.getDocumentPropertyValue(propertyId: documentProperty.id, documentGuid: docGuid, realm) {
                return documentPropertyValue.content
            }
        }
        return nil
    }

    static func getDocFromDp(docType: String, property: String, docGuid: String?, _ realm: Realm) -> Document? {
        if let documentProperty = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: docType, key: property, realm) {
            if docGuid != nil {
                if let documentPropertyValue = DocumentPropertyValue.getDocumentPropertyValue(propertyId: documentProperty.id, documentGuid: docGuid!, realm) {
                    if !documentPropertyValue.documents.isEmpty {
                        return documentPropertyValue.documents.first
                    }
                }
            }
        }
        return nil
    }
    
    static func add2PropertiesArray(_ propertiesArray: [Any], key: String, value: String) -> [Any] {
        var propertiesArray = propertiesArray
        let obj:[String:Any] = ["\(PropertiesConsts.KEY)":key,"\(PropertiesConsts.VALUE)":value]
        propertiesArray.append(obj)
        return propertiesArray
    }
    
    static func add2PropertiesArray(_ propertiesArray: [Any], key: String, value: Int) -> [Any] {
        var propertiesArray = propertiesArray
        let obj:[String:Any] = ["\(PropertiesConsts.KEY)":key,"\(PropertiesConsts.VALUE)":value]
        propertiesArray.append(obj)
        return propertiesArray
    }
    
    static func add2PropertiesArray(_ propertiesArray: [Any], key: String, value: Bool) -> [Any] {
        var propertiesArray = propertiesArray
        let obj:[String:Any] = ["\(PropertiesConsts.KEY)":key,"\(PropertiesConsts.VALUE)":value]
        propertiesArray.append(obj)
        return propertiesArray
    }
    
    static func add2PropertiesArray(_ propertiesArray: [Any], key: String, value: [Any]) -> [Any] {
        var propertiesArray = propertiesArray
        let obj:[String:Any] = ["\(PropertiesConsts.KEY)":key,"\(PropertiesConsts.VALUE)":value]
        propertiesArray.append(obj)
        return propertiesArray
    }
}

