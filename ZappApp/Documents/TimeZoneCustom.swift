//
//  TimeZone.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//
import Foundation
import SwiftyJSON

class TimeZoneCustom {
    
    static let DATE = "date"
    static let TIMEZONE = "timezone"
    static let FORMAT = "format"
    
    // MARK: Properties
    var date: String? = nil
    var timezone: String? = nil
    var format: String? = nil
    
    // MARK: Initialization
    
    init() {
    }
    
    init?(date: String, timezone: String, format: String) {
        // Initialize stored properties.
        self.date = date
        self.timezone = timezone
        self.format = format
    }
    
    static func getTimezone(json: String) -> TimeZoneCustom {
        let tz = TimeZoneCustom()
        let jObject = JSON.init(parseJSON: json)
        if jObject[DATE].exists() {
            tz.date = jObject[DATE].stringValue
        }
        if jObject[TIMEZONE].exists() {
            tz.timezone = jObject[TIMEZONE].stringValue
        }
        if jObject[FORMAT].exists() {
            tz.format = jObject[FORMAT].stringValue
        }
        return tz
    }
}
