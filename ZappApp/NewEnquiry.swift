//
//  Enquiry.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 03.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import CoreLocation

class NewEnquiry {

    //Model for new mc3 enquiry
    
    // MARK: Properties
    var enquiryPhotos: [ImageData]
    var enquiryAddress: String
    var enquiryTypeName: String
    var enquiryComment: String
    var enquiryCallback: Bool
    var enquiryLocation: String
    
    // MARK: Initialization
    
    init?(enquiryPhotos: [ImageData], enquiryAddress: String, enquiryTypeName: String, enquiryComment: String, enquiryLocation: String, enquiryCallback: Bool) {
        // Initialize stored properties.
        self.enquiryPhotos = enquiryPhotos
        self.enquiryAddress = enquiryAddress
        self.enquiryTypeName = enquiryTypeName
        self.enquiryComment = enquiryComment
        self.enquiryCallback = enquiryCallback
        self.enquiryLocation = enquiryLocation
    }
    
    init() {
        self.enquiryPhotos = []
        self.enquiryAddress = ""
        self.enquiryTypeName = ""
        self.enquiryComment = ""
        self.enquiryLocation = ""
        self.enquiryCallback = false
    }
}
