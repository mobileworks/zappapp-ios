//
//  EnquiryLocationViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 05.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import CoreLocation
import RealmSwift

class EnquiryLocationViewController: UIViewController, CLLocationManagerDelegate {

    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    
    var location: CLLocation!
    
    var isLocationDenied = true
    var isWaitingForLocation = false
    
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var postCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        self.address.isHidden = true
        self.city.isHidden = true
        self.postCode.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButton(_ sender: UIBarButtonItem) {
        if self.address.text?.count == 0 && self.city.text?.count == 0 {
            AlertMessage.displayAlertMessage(self, title: "Address missing", message: "Please fill in address first", actionLabel: "ok".localized)
        } else {
            let enquiryAddress = self.address.text! + "\n" + self.city.text! + "\n" + self.postCode.text!
            saveEnquiry(enquiryAddress)
        
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnquirySummaryController")
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    private func locationManager(_ manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined, .denied:
            isLocationDenied = true
        case .authorizedWhenInUse, .authorizedAlways:
            isLocationDenied = false
            locationManager.startUpdatingLocation()
        default:
            break;
        }
    }
    
    @IBAction func getAddressFromGPS(_ sender: UIButton) {
        locationManager(locationManager, didChangeAuthorizationStatus: CLLocationManager.authorizationStatus())
        
        if location != nil {
            fillAddressUsingGeocoding(location)
            self.address.isHidden = false
            self.city.isHidden = false
            self.postCode.isHidden = false
            
            self.address.isEnabled = false
            self.city.isEnabled = false
            self.postCode.isEnabled = false
        } else {
            if isLocationDenied {
                let alert = UIAlertController(title: "locationServicesDisabled".localized, message: "locationAccessRequired".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "allowLocation".localized, style: .cancel, handler: { (alert) -> Void in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                }))
                present(alert, animated: true, completion: nil)
            } else {
                isWaitingForLocation = true
//                AlertMessage.displayAlertMessage(self, title: "Location no available", message: "Can't use GPS location to get address", actionLabel: "ok".localized)
            }
        }
    }
    
    @IBAction func fillAddress(_ sender: UIButton) {
        self.address.isHidden = false;
        self.city.isHidden = false;
        self.postCode.isHidden = false;
        
        self.address.isEnabled = true;
        self.city.isEnabled = true;
        self.postCode.isEnabled = true;
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {        
        location = locations.last
        if isWaitingForLocation {
            fillAddressUsingGeocoding(location)
            self.address.isHidden = false
            self.city.isHidden = false
            self.postCode.isHidden = false
            
            self.address.isEnabled = false
            self.city.isEnabled = false
            self.postCode.isEnabled = false
            isWaitingForLocation = false
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
    }
    
    func fillAddressUsingGeocoding (_ location: CLLocation) {
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, e) -> Void in
            if let _ = e {
                print("Error:  \(e!.localizedDescription)")
            } else {
                let placemark = placemarks!.last! as CLPlacemark
                if let address1 = placemark.thoroughfare {
                    self.address.text = address1 + " " + ((placemark.subThoroughfare != nil) ? placemark.subThoroughfare! : " ")
                }
                self.city.text = placemark.locality
                self.postCode.text = placemark.postalCode
                
            }
        })
    }
    
    func saveEnquiry(_ enquiryAddress : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let enquiry = appDelegate.enquiry
        enquiry.enquiryAddress = enquiryAddress
        
        if location != nil {
            let locValue = location.coordinate;
            enquiry.enquiryLocation = "\(locValue.latitude),\(locValue.longitude)"
        }
    }
}
