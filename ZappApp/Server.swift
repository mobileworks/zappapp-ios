//
//  Server.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit

class Server {
    
    // MARK: Properties
    var key: String
    var name: String
    var url: String
    var hidden: Bool
    
    // MARK: Initialization
    
    init?(key: String, name: String, url: String, hidden: Bool) {
        // Initialize stored properties.
        self.key = key
        self.name = name
        self.url = url
        self.hidden = hidden
    }
}
