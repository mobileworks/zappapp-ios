//
//  Database.swift
//  StopAlert
//
//  Created by Pawel Figiel on 27.02.2016.
//  Copyright © 2016 Pawel Figiel. All rights reserved.
//

import UIKit
import SwiftyJSON

class CurrentUser{
    
    let objectKey = "currentUser"
    let defaults = UserDefaults.standard
    
    func getUser() -> User?{
        if let sessionUser = defaults.object(forKey: "currentUser") as? NSData{
            if let storedUser = NSKeyedUnarchiver.unarchiveObject(with: sessionUser as Data) as? User{
                return storedUser
            }
        }
        return nil
    }
    
    func storeUser(userParams: JSON){
        var workerGuid: String? = nil
        if userParams[User.WORKER_GUID].exists() {
            workerGuid = userParams[User.WORKER_GUID].stringValue
        }
        
        let currentUser = User(guid: userParams[User.GUID].stringValue, userName: userParams[User.USER_NAME].stringValue, displayName: userParams[User.DISPLAY_NAME].stringValue, secretKey: userParams[User.SECRET_KEY].stringValue, workerGuid: workerGuid!)
        defaults.set(NSKeyedArchiver.archivedData(withRootObject: currentUser), forKey: self.objectKey)
        defaults.synchronize()
    }
    
    func deleteUser(){
        defaults.removeObject(forKey: self.objectKey)
        defaults.synchronize()
    }
}
