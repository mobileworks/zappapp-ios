//
//  RuleSet.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class RuleSet: Object {
    
    @objc dynamic var propertyId : String = "" //link to property id
    @objc dynamic var nullable = false //if property has nullable set to true it accepts null values
    @objc dynamic var order = false // order no
    @objc dynamic var orderInt = 0
    @objc dynamic var hidden = false //if it is true than the property is hidden if false it is visible
    @objc dynamic var lazy_loading = false //do not load all documents for this property, but wait for the user to add a few first character
    @objc dynamic var creatable = false //this property can be edited when the document is created
    @objc dynamic var editable = false //this property can be edited when the document already exists
    @objc dynamic var accessible = false
    
    override static func primaryKey() -> String? {
        return PROPERTY_ID
    }
    
    static let PROPERTY_ID = "propertyId"
    static let NULLABLE = "nullable"
    static let ORDER = "order"
    static let HIDDEN = "hidden"
    static let LAZY_LOADING = "lazy_loading"
    static let CREATABLE = "creatable"
    static let EDITABLE = "editable"
    static let ACCESSIBLE = "accessible"
    
    static func getRuleSet(propertyId: String, realm: Realm) -> RuleSet? {
        return realm.objects(RuleSet.self).filter(NSPredicate(format: "propertyId = %@", propertyId)).first
    }
    
    static func newRuleSet() -> RuleSet {
        return RuleSet()
    }
    
    static func createNewRuleSet(jObject: JSON, propertyId: String, _ realm: Realm) -> RuleSet {
        let ruleSet = RuleSet()
        ruleSet.propertyId = propertyId
        ruleSet.nullable = jObject[NULLABLE].boolValue
        let order = jObject[ORDER].rawValue
        if order is Int {
            ruleSet.orderInt = order as! Int
            ruleSet.order = true
        }
        ruleSet.hidden = jObject[HIDDEN].boolValue
        ruleSet.creatable = jObject[CREATABLE].boolValue
        ruleSet.editable = jObject[EDITABLE].boolValue
        ruleSet.lazy_loading = jObject[LAZY_LOADING].boolValue
        if jObject[ACCESSIBLE].exists() {
            ruleSet.accessible = jObject[ACCESSIBLE].boolValue
        }
        return ruleSet
    }
    
    static func updateRuleSet(jObject: JSON, propertyId: String, _ realm: Realm) {
        if let ruleSet = RuleSet.getRuleSet(propertyId: propertyId, realm: realm) {
            ruleSet.nullable = jObject[NULLABLE].boolValue
            let order = jObject[ORDER].rawValue
            if order is Int {
                ruleSet.orderInt = order as! Int
                ruleSet.order = true
            }
            ruleSet.hidden = jObject[HIDDEN].boolValue
            ruleSet.creatable = jObject[CREATABLE].boolValue
            ruleSet.editable = jObject[EDITABLE].boolValue
            ruleSet.lazy_loading = jObject[LAZY_LOADING].boolValue
            if jObject[ACCESSIBLE].exists() {
                ruleSet.accessible = jObject[ACCESSIBLE].boolValue
            }
        }
    }
}

