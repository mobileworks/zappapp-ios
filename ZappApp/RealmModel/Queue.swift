//
//  Queue.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import RealmSwift

class Queue: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var method: Int = 0
    @objc dynamic var params: NSData?
    @objc dynamic var endpoint: String?
    @objc dynamic var content: NSData?
    @objc dynamic var urlQueryString: String?
    @objc dynamic var executeImmediately = false
    @objc dynamic var parentGuid: String?
    @objc dynamic var parentGuidKey: String?
    @objc dynamic var streetGuid: String?
    @objc dynamic var targetKey: String?
    @objc dynamic var documentGuid: String?
    @objc dynamic var tryCounter = 0
    @objc dynamic var tempDocumentGuid: String?
    @objc dynamic var sendFromDocumentProperty = 0
    
    override static func primaryKey() -> String? {
        return ID
    }
    
    override static func indexedProperties() -> [String] {
        return [METHOD, DOCUMENT_GUID]
    }
    
    static let ID = "id"
    static let METHOD = "method"
    static let PARAMS = "params"
    static let ENDPOINT = "endpoint"
    static let CONTENT = "content"
    static let URL_QUERY_STRING = "urlQueryString"
    static let EXECUTE_IMMEDIATELY = "executeImmediately"
    static let PARENT_GUID = "parentGuid"
    static let PARENT_GUID_KEY = "parentGuidKey"
    static let STREET_GUID = "streetGuid"
    static let TARGET_KEY = "targetKey"
    static let DOCUMENT_GUID = "documentGuid"
    static let TRY_COUNTER = "tryCounter"
    static let TEMP_DOCUMENT_GUID = "tempDocumentGuid"
    static let SEND_FROM_DOCUMENT_PROPERTY = "sendFromDocumentProperty"
    
    func getParams() -> NSData? {
        return params
    }
    
    static func newQueue() -> Queue {
        return Queue()
    }
    
    static func getQueue(id: Int, _ realm: Realm) -> Queue? {
        let results = realm.objects(Queue.self)
        if !results.isEmpty {
            print("\(results.count)")
        }
        return realm.objects(Queue.self).filter("\(ID) == \(id)").first
    }
    
    static func getAllQueue(_ realm: Realm) -> Results<Queue> {
        return realm.objects(Queue.self).filter("\(METHOD) != 0").sorted(byKeyPath: ID)
    }
    
    static func getAllQueueWithIds(_ realm: Realm) -> Results<Queue> {
        return realm.objects(Queue.self).filter("\(METHOD) != 0").filter("\(DOCUMENT_GUID) != nil").sorted(byKeyPath: ID)
    }
    
    static func getAllQueuesByIds(idsList: [Int], _ realm: Realm) -> Results<Queue> {
        return realm.objects(Queue.self).filter("\(DOCUMENT_GUID) != nil").filter(NSPredicate(format: "\(ID) IN %@", idsList)).sorted(byKeyPath: ID)
    }
    
    static func isQueueEmpty(lowerLimit: Int, _ realm: Realm) -> Bool {
        let realms = realm.objects(Queue.self).filter("\(METHOD) != 0").filter("\(ID) > \(lowerLimit)").sorted(byKeyPath: ID)
        return realms.isEmpty
    }
    
    static func getAllQueueCount(_ realm: Realm) -> Int64 {
        return Int64(realm.objects(Queue.self).filter("\(METHOD) != 0").count)
    }
    
    static func updateParentGuid(parentGuid: String, queueMessageId: Int, _ realm: Realm) {
        let results = realm.objects(Queue.self).filter(NSPredicate(format: "\(PARENT_GUID) = %@", "\(queueMessageId)"))
        if !results.isEmpty {
            for queue in results {
                queue.parentGuid = parentGuid
            }
        }
    }
    
    static func checkIfMessageExistsOnQueue(targetKey: String, parentGuid: String, _ realm: Realm) -> Bool {
        return checkIfMessageExistsOnQueue(targetKey: targetKey, parentGuid: parentGuid, documentGuid: nil, realm: realm);
    }
    
    static func checkIfMessageExistsOnQueue(targetKey: String, parentGuid: String, documentGuid: String?, realm: Realm) -> Bool {
        let query = realm.objects(Queue.self).filter(NSPredicate(format: "\(TARGET_KEY) = %@", "\(targetKey)")).filter(NSPredicate(format: "\(PARENT_GUID) = %@", "\(parentGuid)"))
        if documentGuid != nil {
            let query2 = query.filter(NSPredicate(format: "\(DOCUMENT_GUID) = %@", "\(documentGuid)"))
            return !query2.isEmpty
        }
        return !query.isEmpty
    }
    
    //ZappApp
    static func getAllQueueElementsByDocumentGuid(documentGuid: String, _ realm: Realm) -> Int64 {
        return Int64(realm.objects(Queue.self).filter(NSPredicate(format: "\(DOCUMENT_GUID) != %@", "\(documentGuid)")).count)
    }
    
    static func cleanAllQueue(_ realm: Realm) {
        try! realm.write {
            let docs = realm.objects(Queue.self)
            if !docs.isEmpty {
                realm.delete(docs)
            }
        }
    }
    
    static func delete(id: Int, _ realm: Realm) {
        print("queue counter \(getAllQueueCount(realm))")
        let queueRealm = realm.objects(Queue.self).filter("\(ID) = \(id)")
        if realm.isInWriteTransaction {
            realm.delete(queueRealm)
        } else {
            try! realm.write {
                realm.delete(queueRealm)
            }
        }
        print("queue counter \(getAllQueueCount(realm))")
    }
}
