//
//  DocumentPropertyValue.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import RealmSwift

class DocumentPropertyValue: Object {
    
    @objc dynamic var dpvId: String?
    @objc dynamic var propertyId = 0
    @objc dynamic var content: String?
    @objc dynamic var additionalContent: String?
    @objc dynamic var contentDate : Date?
    
    let documents = List<Document>()
    
    override static func primaryKey() -> String? {
        return DPV_ID
    }
    
    override static func indexedProperties() -> [String] {
        return [PROPERTY_ID]
    }
    
    static let DPV_ID = "dpvId"
    static let PROPERTY_ID = "propertyId"
    static let CONTENT = "content"
    static let ADDITIONAL_CONTENT = "additionalContent"
    static let CONTENT_DATE = "contentDate"
    static let DOCUMENTS = "documents"
    
    func documentGuidFromDpv() -> String? {
        let split = self.dpvId?.components(separatedBy: "_")
        if split?.count == 2 {
            return (split?[1])!
        } else {
            return nil
        }
    }
    
    static func newDocumentPropertyValue() -> DocumentPropertyValue {
        return DocumentPropertyValue()
    }
    
    static func getDocumentPropertyValuesByDocumentGuid(documentGuid: String, _ realm: Realm) -> List<DocumentPropertyValue> {
        
        let doc = realm.objects(Document.self).filter(NSPredicate(format: "\(Document.GUID) = %@", "\(documentGuid)")).first
        if doc == nil {
            return List<DocumentPropertyValue>()
        } else {
            return doc!.propertyValues
        }
    }
    
    static func getDocumentPropertyValueByPropertyId(propertyId: Int, properties: List<DocumentPropertyValue>) -> DocumentPropertyValue? {
        return properties.filter("\(PROPERTY_ID) = \(propertyId)").first
    }
    
    static func getDocumentPropertyValuesByParentParentGuidList(guidList: [String], _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter(NSPredicate(format: "ANY \(DOCUMENTS).\(Document.GUID) IN %@", guidList))
    }
    
    static func getDocumentPropertyValue(propertyId: Int, _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)")
    }
    
    static func getDocumentPropertyValueByDpvId(dpvId: String, _ realm: Realm) -> DocumentPropertyValue? {
        return realm.objects(DocumentPropertyValue.self).filter(NSPredicate(format: "\(DPV_ID) = %@", "\(dpvId)")).first
    }
    
    static func getDocumentPropertyValue(propertyId: Int, documentGuid: String, _ realm: Realm) -> DocumentPropertyValue? {
        let dpvList = realm.objects(DocumentPropertyValue.self).filter(NSPredicate(format: "\(DPV_ID) = %@", "\(propertyId)_\(documentGuid)"))
        if dpvList.isEmpty {
            //should be nil check
            return nil
        } else {
            return dpvList.first!
        }
    }
    
    static func getDocumentPropertyValueAdditionalContent(propertyId: Int, documentGuid: String, _ realm: Realm) -> String? {
        if let dpv = getDocumentPropertyValue(propertyId: propertyId, documentGuid: documentGuid, realm) {
            return dpv.additionalContent
        } else {
            return nil
        }
    }
    
    //    public static SparseArray<DbDocumentPropertyValue> getDocumentPropertyValue(String documentGuid, Realm realm) {
    //    SparseArray<DbDocumentPropertyValue> l = new SparseArray<>();
    //
    //    List<DbDocumentPropertyValue> list = realm.where(DbDocument.class).equalTo(DbDocument.GUID, documentGuid).findFirst().getPropertyValues();
    //
    //    for (DbDocumentPropertyValue dpv : list) {
    //    l.append(dpv.propertyId, dpv);
    //    }
    //    return l;
    //    }
    //
    //    public static byte[] getBLOBContent(int propertyId, String documentGuid, Realm realm) {
    //    DbDocument doc = realm.where(DbDocument.class).equalTo(DbDocument.GUID, documentGuid).findFirst();
    //    if (doc != null) {
    //    DbDocumentPropertyValue dpv = doc.getPropertyValues().where().equalTo(PROPERTY_ID, propertyId).findFirst();
    //    if (dpv != null) {
    //    String content = dpv.getContent();
    //    if (content != null) {
    //    return dpv.content.getBytes();
    //    }
    //    }
    //    }
    //    return null;
    //    }
    
    static func getDpvFromDp(docType: String, key: String, docGuid: String, _ realm: Realm) -> DocumentPropertyValue? {
        let dp = realm.objects(DocumentProperty.self).filter(NSPredicate(format: "\(DocumentProperty.DOCUMENT_TYPE).\(DocumentType.NAME) = %@", "\(docType)")).filter(NSPredicate(format: "\(DocumentProperty.KEY) = %@", key)).first
        if dp != nil {
            return realm.objects(DocumentPropertyValue.self).filter(NSPredicate(format: "\(DPV_ID) = %@", "\(dp!.id)_\(docGuid)")).first
        } else {
            return nil
        }
    }
    
    static func getDocumentPropertyValueByPropertyGuidContent(propertyId: Int, content: String, _ realm: Realm) -> DocumentPropertyValue? {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)").filter(NSPredicate(format: "\(CONTENT) = %@", content)).first
    }
    
    static func getDpvByPropertyGuidContent(propertyId: Int, content: String, _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)").filter(NSPredicate(format: "\(CONTENT) = %@", content))
    }
    
    static func getDpvByPropertyGuidContentLike(propertyId: Int, content: String, _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)").filter(NSPredicate(format: "\(CONTENT) CONTAINS[c] %@", content))
    }
    
    static func getDpvByPropertyGuidContentBetween(propertyId: Int, dateFrom: Date, dateTo: Date, _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)").filter(NSPredicate(format: "\(CONTENT) = %@", [dateFrom, dateTo]))
    }
    
    static func getDpvByPropertyDocuments(propertyId: Int, documentsGuid: [String], _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)").filter(NSPredicate(format: "ANY \(DOCUMENTS).\(Document.GUID) IN %@",documentsGuid))
    }
    
    static func getDpvByPropertyDocuments(propertyId: Int, documentGuid: String, _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(propertyId)").filter(NSPredicate(format: "ANY \(DOCUMENTS).\(Document.GUID) = %@",documentGuid))
    }
    
    static func getDpvByDocument(documentGuid: String, _ realm: Realm) -> Results<DocumentPropertyValue> {
        return realm.objects(DocumentPropertyValue.self).filter(NSPredicate(format: "ANY \(DOCUMENTS).\(Document.GUID) = %@",documentGuid))
    }
    
    static func getDocumentGuidsForFilterByValues(documentGuid: String, documentPropertyType: String, allowedTypes: List<DocumentType>, _ realm: Realm) -> [(key: String?, value: String)] {
        var labels = [(key: String?, value: String)]()
        if let dp = DocumentProperty.getDocumentPropertyByDocumentTypesKey(docTypes: allowedTypes, key: documentPropertyType, realm: realm) {
            let ddd = realm.objects(DocumentPropertyValue.self).filter("\(PROPERTY_ID) = \(dp.id)").filter(NSPredicate(format: "ANY \(DOCUMENTS).\(Document.GUID) = %@",documentGuid))
            for dd in ddd {
                if let doc = dd.documentGuidFromDpv() {
                    if let docSingle = Document.getDocument(guid: doc, realm) {
                        labels.append((doc, docSingle.listName!))
                    }
                }
            }
        }
        return labels
    }
    
    static func deleteByGuid(documentGuid: String, _ realm: Realm) {
        if let document = realm.objects(Document.self).filter(NSPredicate(format: "\(Document.GUID) = %@",documentGuid)).first {
            let dpvList = document.propertyValues
            realm.delete(dpvList)
            print("All document property values DELETED");
        }
    }
    
    static func deleteAll(_ realm: Realm) {
        let docs = realm.objects(DocumentPropertyValue.self)
        if !docs.isEmpty {
            realm.delete(docs)
            print("All document property values DELETED");
        }
    }
}

