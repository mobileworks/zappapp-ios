//
//  DocumentType.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import RealmSwift

class DocumentType: Object {
    
    @objc dynamic var name: String?
    @objc dynamic var translation: String?
    @objc dynamic var lastModified: Int64 = 0
    @objc dynamic var groups: String?
    @objc dynamic var asset = false
    @objc dynamic var listProperty: String?
    @objc dynamic var module: String?
    let properties = List<DocumentProperty>()
    
    override static func primaryKey() -> String? {
        return NAME
    }
    
    static let NAME = "name"
    static let TRANSLATION = "translation"
    static let LAST_MODIFIED = "lastModified"
    static let GROUPS = "groups"
    static let ASSET = "asset"
    static let LIST_PROPERTY = "listProperty"
    static let MODULE = "module"
    static let PROPERTIES = "properties"
    
    static func newDocumentType() -> DocumentType {
        return DocumentType()
    }
    
    static func getDocumentTypes(modules: [String], _ realm: Realm) -> Results<DocumentType> {
        var result: Results<DocumentType>? = nil
        if !modules.isEmpty {
            result = realm.objects(DocumentType.self).filter(NSPredicate(format: "\(MODULE) IN %@", modules)).sorted(byKeyPath: NAME)
        }
        return result!
    }
    
    static func getDocumentTypesCountAll(_ realm: Realm) -> Int {
        return realm.objects(DocumentType.self).count
    }
    
    static func getDocumentTypesCountAssets(_ realm: Realm) -> Int {
        return realm.objects(DocumentType.self).filter(NSPredicate(format: "\(ASSET) = %@", true as CVarArg)).count
    }
    
    static func isDocumentTypeAnAsset(docType: String, _ realm: Realm) -> Bool {
        let results = realm.objects(DocumentType.self).filter(NSPredicate(format: "\(NAME) = %@ AND \(ASSET) = %@", docType, true as CVarArg)).first
        return results != nil
    }
    
    static func setLastModified(name: String, lastModified: Int64, _ realm: Realm) {
        if let results = realm.objects(DocumentType.self).filter(NSPredicate(format: "\(NAME) = %@", "\(name)")).first {
            results.lastModified = lastModified
        }
    }
    
    static func getLastModified(name: String, _ realm: Realm) -> Int64? {
        if let results = realm.objects(DocumentType.self).filter(NSPredicate(format: "\(NAME) = %@", "\(name)")).first {
            return results.lastModified
        } else {
            return 0
        }
    }
    
    static func getDocumentType(docType: String, _ realm: Realm) -> DocumentType? {
        return realm.objects(DocumentType.self).filter(NSPredicate(format: "\(NAME) = %@", "\(docType)")).first
    }
    
    static func getDocumentTypesAll(_ realm: Realm) -> Results<DocumentType> {
        return realm.objects(DocumentType.self)
    }
    
    static func getTabName(_ docType: String, _ realm: Realm) -> String {
        var name = ""
        if let document = DocumentType.getDocumentType(docType: docType, realm) {
            name = document.translation!
        }
        return name
    }
    
    static func deleteAll(_ realm: Realm) {
        let docs = realm.objects(DocumentType.self)
        if !docs.isEmpty {
            realm.delete(docs)
            print("All documents DELETED");
        }
        print("All document types DELETED");
    }
}
