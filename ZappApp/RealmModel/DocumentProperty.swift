//
//  DocumentProperty.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import RealmSwift

class DocumentProperty: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var documentType: DocumentType?
    @objc dynamic var key: String?
    @objc dynamic var name: String?
    @objc dynamic var group: String? = nil
    @objc dynamic var guid: String?
    @objc dynamic var ruleSet: RuleSet?
    @objc dynamic var dynamic = false
    @objc dynamic var classNameStr: String?
    let allowedDocumentTypes = List<DocumentType>()
    @objc dynamic var params: String? = nil
    @objc dynamic var filters: String? = nil
    
    override static func primaryKey() -> String? {
        return ID
    }
    
    override static func indexedProperties() -> [String] {
        return [KEY]
    }
    
    static let ID = "id"
    static let DOCUMENT_TYPE = "documentType"
    static let KEY = "key"
    static let NAME = "name"
    static let GROUP_VALUE = "groupValue"
    static let GUID = "guid"
    static let RULE_SET = "ruleSet"
    static let DYNAMIC = "dynamic"
    static let ALLOWED_DOCUMENT_TYPES = "allowedDocumentTypes"
    static let PARAMS = "params"
    static let FILTERS = "filters"
    
    func ruleSetString() {
        print("Ruleset for \((self.documentType?.name)!), \((self.key)!) = \((self.ruleSet.map({$0}))!)")
    }
    
    func tag() -> String {
        return "\((self.documentType?.name)!)_\((self.key)!)_"
    }
    
    func createTag(_ view: String) -> String {
        return "\(tag())\(view.lowercased())"
    }
    
    static func newDocumentProperty() -> DocumentProperty {
        return DocumentProperty()
    }
    
    static func getDocumentProperties(_ realm: Realm) -> Results<DocumentProperty> {
        return realm.objects(DocumentProperty.self)
    }
    
    static func getDocumentPropertyById(id: Int, _ realm: Realm) -> DocumentProperty? {
        return realm.objects(DocumentProperty.self).filter("\(ID) = \(id)").first
    }
    
    static func getDocumentPropertiesByDocumentType(docType: String, _ realm: Realm) -> Results<DocumentProperty> {
        return realm.objects(DocumentProperty.self).filter(NSPredicate(format: "\(DOCUMENT_TYPE).\(DocumentType.NAME) = %@", "\(docType)")).sorted(byKeyPath: ID, ascending: true)
    }
    
    static func getDocumentPropertiesByDocumentTypeList(docType: String, _ realm: Realm) -> [DocumentProperty] {
        
        let realmsList = realm.objects(DocumentProperty.self).filter(NSPredicate(format: "\(DOCUMENT_TYPE).\(DocumentType.NAME) = %@", "\(docType)"))
        var results = [DocumentProperty]()
        for realmS in realmsList {
            results.append(realmS)
        }
        return results
    }
    
    static func getDocumentPropertyByDocumentTypeKey(docType: String, key: String, _ realm: Realm) -> DocumentProperty? {
        return realm.objects(DocumentProperty.self).filter(NSPredicate(format: "\(KEY) = %@", "\(key)")).filter(NSPredicate(format: "\(DOCUMENT_TYPE).\(DocumentType.NAME) = %@", "\(docType)")).first
    }
    
    static func getDocumentPropertyByDocumentTypesKey(docTypes: List<DocumentType>, key: String, realm: Realm) -> DocumentProperty? {
        var dt = [String]()
        
        for docType in docTypes {
            dt.append(docType.name!)
        }
        
        return realm.objects(DocumentProperty.self).filter(NSPredicate(format: "\(KEY) = %@", key)).filter(NSPredicate(format: "\(DOCUMENT_TYPE).\(DocumentType.NAME) IN %@", dt)).first
    }
    
    static func getDOcumentPropertyCount(_ realm: Realm) -> Int {
        return realm.objects(DocumentProperty.self).count
    }
    
    static func deleteAll(_ realm: Realm) {
        let docs = realm.objects(DocumentProperty.self)
        if !docs.isEmpty {
            realm.delete(docs)
            print("All document properties DELETED");
        }
    }
}

