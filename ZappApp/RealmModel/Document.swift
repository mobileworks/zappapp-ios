//
//  Document.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import RealmSwift

class Document: Object {
    
    @objc dynamic var guid: String?
    @objc dynamic var type: DocumentType?
    @objc dynamic var lastModified : Int64 = 0
    @objc dynamic var availableActions : String?
    @objc dynamic var created: String?
    @objc dynamic var updated: String?
    @objc dynamic var listName: String?
    @objc dynamic var module: String?
    @objc dynamic var status: Int = 0
    let propertyValues = List<DocumentPropertyValue>()
    
    override static func primaryKey() -> String? {
        return GUID
    }
    
    static let GUID = "guid"
    static let TYPE = "type"
    static let LAST_MODIFIED = "lastModified"
    static let AVAILABLE_ACTIONS = "availableActions"
    static let CREATED = "created"
    static let UPDATED = "updated"
    static let LIST_NAME = "listName"
    static let MODULE = "module"
    static let PROPERTY_VALUES = "propertyValues"
    static let STATUS = "status"
    
    static func newDocument() -> Document {
        return Document()
    }
    
    static func getDocumentsByGuids(guidsList: [String], _ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self).filter(NSPredicate(format: "\(GUID) IN %@", guidsList))
    }
    
    static func getDocuments(_ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self)
    }
    
    static func getDocument(guid: String, _ realm: Realm) -> Document? {
        let doc = realm.objects(Document.self).filter(NSPredicate(format: "\(GUID) = %@", "\(guid)"))
        if !doc.isEmpty {
            return doc.first
        }
        return nil
    }
    
    static func getDocumentUsingStatus(guid: String, status: Int, _ realm: Realm) -> Document? {
        let doc = realm.objects(Document.self).filter(NSPredicate(format: "\(GUID) = %@ AND \(STATUS) = %@", "\(guid)", status))
        if !doc.isEmpty {
            return doc.first
        }
        return nil
    }
    
    static func getDocumentsByTypeAndStatus(type: String, status: Int, _ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) = %@ AND \(STATUS) = %@", "\(type)", status))
    }
    
    static func getDocumentsByType(type: String, _ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) = %@", "\(type)")).sorted(byKeyPath: LIST_NAME)
    }
    
    static func getDocumentsByTypeAndPrefix(type: String, prefix: String, _ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) = %@ AND \(GUID) BEGINSWITH %@", type, prefix))
    }
    
    static func checkIfAddDocument2Queue(documentGuid: String, _ realm: Realm) -> Int8 {
        //0 exists on device
        //1 in queue
        //2 neither in queue or on device
        
        let doc = realm.objects(Document.self).filter(NSPredicate(format: "\(GUID) = %@", "\(documentGuid)"))
        if doc.isEmpty {
            let queueRealm = realm.objects(Queue.self).filter(NSPredicate(format: "\(Queue.DOCUMENT_GUID) = %@", "\(documentGuid)"))
            if queueRealm.isEmpty {
                return 2
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    
    static func getDocumentByPropertyIdAndDocuments(propertyId: Int, documentGuids: [String], _ realm: Realm) -> Results<Document> {
        let xx = realm.objects(Document.self)
            .filter(NSPredicate(format: "\(PROPERTY_VALUES).\(DocumentPropertyValue.PROPERTY_ID) = \(propertyId)"))
            .filter(NSPredicate(format: "\(GUID) IN %@", documentGuids))
        return xx
    }
    
    static func getDocumentsByTypeFilterByPropertyKeyContent(type: String, propertyKey: String, content: String, _ realm: Realm) -> Results<Document>? {
        if let property = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: type, key: propertyKey, realm) {
            let dpv = DocumentPropertyValue.getDpvByPropertyGuidContent(propertyId: property.id, content: content, realm)
            var ss = [String]()
            for item in dpv {
                ss.append(item.documentGuidFromDpv()!)
            }
            if !dpv.isEmpty {
                return realm.objects(Document.self).filter(NSPredicate(format: "\(Document.GUID) IN %@", ss)).sorted(byKeyPath: LAST_MODIFIED)
            }
        }
        return nil
    }
    
    static func getDocumentsByTypeFilterByPropertyKeyDocGuid(type: String, propertyKey: String, docGuid: String, _ realm: Realm) -> Results<Document>? {
        if let property = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: type, key: propertyKey, realm) {
            let dpv = DocumentPropertyValue.getDpvByPropertyDocuments(propertyId: property.id, documentGuid: docGuid, realm)
            var ss = [String]()
            for item in dpv {
                ss.append(item.documentGuidFromDpv()!)
            }
            if !dpv.isEmpty {
                return realm.objects(Document.self).filter(NSPredicate(format: "\(Document.GUID) IN %@", ss)).sorted(byKeyPath: LAST_MODIFIED)
            }
        }
        return nil
    }
    
    static func getDocumentsByTypeFilterByContent(type: String, content: String, _ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) = %@", "\(type)")).filter(NSPredicate(format: "\(PROPERTY_VALUES).\(DocumentPropertyValue.CONTENT) IN %@", "\(content)"))
    }
    
    static func getDocumentsByTypes(types: List<DocumentType>, _ realm: Realm) -> Results<Document>? {
        
        var documentTypes = [String]()
        for type in types {
            documentTypes.append(type.name!)
        }
        if !documentTypes.isEmpty {
            return realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) IN %@", documentTypes)).sorted(byKeyPath: LIST_NAME)
        }
        return nil
    }
    
    
    static func getDocumentPropertyValuesByTypeByFilteringPropertyValueContent(type: String, filteredContent: String, _ realm: Realm) -> Results<Document> {
        return realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) = %@ AND \(LIST_NAME) CONTAINS %@", "\(type)", "\(filteredContent)")).sorted(byKeyPath: LAST_MODIFIED, ascending: false)
    }
    
    static func getListName(guid: String, _ realm: Realm) -> String {
        if let document = realm.objects(Document.self).filter(NSPredicate(format: "\(GUID) = %@", "\(guid)")).first {
            return document.listName!
        }
        return ""
    }
    
    static func getAllDocumentCount(_ realm: Realm) -> Int {
        return realm.objects(Document.self).count
    }
    
    static func deleteByGuid(guid: String, _ realm: Realm) {
        if let doc = realm.objects(Document.self).filter(NSPredicate(format: "\(GUID) = %@", "\(guid)")).first {
            realm.delete(doc)
        }
    }
    
    static func deleteByDocumentType(typeName: String, _ realm: Realm) {
        let docs = realm.objects(Document.self).filter(NSPredicate(format: "\(TYPE).\(DocumentType.NAME) = %@", "\(typeName)"))
        if !docs.isEmpty {
            realm.delete(docs)
            DocumentType.setLastModified(name: typeName, lastModified: 0, realm)
        }
    }
    
    static func deleteAll(_ realm: Realm) {
        let docs = realm.objects(Document.self)
        if !docs.isEmpty {
            realm.delete(docs)
            print("All documents DELETED");
        }
    }
}

