//
//  MapPointView.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 29.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import Foundation
import MapKit

class MapPointView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard (newValue as? MapPoint) != nil else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = "xx"//artwork.address
            detailCalloutAccessoryView = detailLabel
        }
    }    
}

