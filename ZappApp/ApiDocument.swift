//
//  ApiDocumentType.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//


import Alamofire
import JWT
import SwiftyJSON

class ApiDocument: Api{
    
    func getList(params: Parameters, endPoint: String, queue: DispatchQueue, completion:@escaping (_ finished: Bool, _ response: JSON?) -> Void) {
        super.call(true, method: HTTPMethod.get, endpoint: endPoint, content: nil, params: params, senderParam: 0, queue: queue, completion:completion)
    }
    
    func create(content: Parameters?, endPoint: String, senderParam: Int, queue: DispatchQueue, completion:@escaping (_ finished: Bool, _ response: JSON?) -> Void) {
        super.call(true, method: HTTPMethod.post, endpoint: endPoint, content: content, params: nil, senderParam: senderParam, queue: queue, completion:completion)
    }
    
    func update(content: Parameters?, endPoint: String, senderParam: Int, queue: DispatchQueue, completion:@escaping (_ finished: Bool, _ response: JSON?) -> Void) {
        super.call(true, method: HTTPMethod.put, endpoint: endPoint, content: content, params: nil, senderParam: senderParam, queue: queue, completion:completion)
    }
}
