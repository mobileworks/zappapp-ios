//
//  CustomPhotoGalleryPopOverView.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 26.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation

protocol PhotoGaleryDelegate: class {
    func getPhotos(photos: [ImageData]?)
}

class CustomPhotoGalleryPopOverView: UIStackView, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, PhotoListAlertDelegate {
    weak var delegate: PhotoGaleryDelegate?
    
    var documentGuid: String? = nil
    var property: DocumentProperty? = nil
    var controller: UIViewController? = nil
    
    var photoView:UIStackView? = nil
    var collectionView: PhotoCollectionView? = nil
    
    var list = [ImageData]()
    
    init(controller: UIViewController, property: DocumentProperty, documentGuid: String?, list: [ImageData]) {
        super.init(frame: UIScreen.main.bounds)
        
        self.controller = controller
        self.property = property
        
        self.list = list
        self.documentGuid = documentGuid
        
        let mainView = UIStackView()
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.axis = .vertical
        mainView.distribution = .fill
        mainView.alignment = .fill
        mainView.spacing = 5
        
        addArrangedSubview(mainView)
        
        let buttonView = UIStackView()
        buttonView.translatesAutoresizingMaskIntoConstraints = false
        buttonView.axis = .horizontal
        buttonView.distribution = .fillEqually
        buttonView.alignment = .fill
        buttonView.spacing = 5
        
        mainView.addArrangedSubview(buttonView)
        
        let buttonTake = UIButton()
        buttonTake.setTitleColor(buttonTake.tintColor, for: .normal)
        buttonTake.setTitle("takePhoto".localized, for: UIControl.State.normal)
        buttonTake.translatesAutoresizingMaskIntoConstraints = false
        buttonTake.addTarget(self, action:#selector(clickTake), for:.touchUpInside)
        buttonView.addArrangedSubview(buttonTake)
        
        let buttonUpload = UIButton()
        buttonUpload.setTitleColor(buttonUpload.tintColor, for: .normal)
        buttonUpload.setTitle("uploadPhoto".localized, for: UIControl.State.normal)
        buttonUpload.translatesAutoresizingMaskIntoConstraints = false
        buttonUpload.addTarget(self, action:#selector(clickLoad), for:.touchUpInside)
        buttonView.addArrangedSubview(buttonUpload)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        let xx: Int = Int(UIScreen.main.bounds.width)/300
        if xx > 1 {
            let yy = Int(UIScreen.main.bounds.width)/xx - 20
            layout.itemSize = CGSize(width: yy, height: yy)
        } else {
            let yy = Int(UIScreen.main.bounds.width) - 20
            layout.itemSize = CGSize(width: yy, height: yy)
        }
        
        collectionView = PhotoCollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView?.backgroundColor = UIColor.white
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        
        mainView.addArrangedSubview(collectionView!)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func clickLoad(sender:UIButton!) {
        sender.resignFirstResponder()
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        imagePickerController.definesPresentationContext = true
        imagePickerController.modalPresentationStyle = .overCurrentContext
        controller?.navigationController?.present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func clickTake(sender:UIButton!) {
        sender.resignFirstResponder()
        checkCamera()
    }
    
    func callCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.definesPresentationContext = true
        imagePicker.modalPresentationStyle = .overCurrentContext
        controller?.navigationController?.present(imagePicker, animated: false, completion: nil)
    }
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch authStatus {
        case .authorized: callCamera()
        case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(title: "", message: "cameraAccessRequired".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "allowCamera".localized, style: .cancel, handler: { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }))
        controller?.present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) -> Void in
            DispatchQueue.main.async() {
                self.checkCamera()
            }
        })
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            //save image
            //display image
            let screenSize: CGRect = UIScreen.main.bounds
            
            let imageView = UIImageView()
            
            let imageres = self.resizeImage(image: image, targetSize: CGSize(width: screenSize.width, height: screenSize.width))
            
            imageView.image = imageres
            
            let imageData = ImageData()
            imageData?.imageView = imageView
            
            let formatter = DateFormatter()
            formatter.dateFormat = Constants.PHOTO_DATETIME_FORMAT
            
            imageData?.date = formatter.string(from: Date())
            
            list.append(imageData!)
            collectionView?.reloadData()
            delegate?.getPhotos(photos: list)
        }
        controller?.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        controller?.dismiss(animated: true, completion: nil)
    }
    
    func displayAlertMessage(delegate: UIViewController, title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let alertActionNo = UIAlertAction(title: "no".localized, style: UIAlertAction.Style.default, handler: nil)
        alertController.addAction(alertActionNo)
        
        let alertActionYes = UIAlertAction(title: "yes".localized, style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) in
        }
        
        alertController.addAction(alertActionYes)
        
        controller?.present(alertController, animated:true, completion:nil)
    }
    
    func getValue(row: IndexPath) {
        list.remove(at: row.row)
        collectionView?.deleteItems(at: [row])
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: (size.width * heightRatio), height: (size.height * heightRatio))
        } else {
            newSize = CGSize(width: (size.width * widthRatio), height: (size.height * widthRatio))
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.backgroundView = list[indexPath.row].imageView
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageData = list[indexPath.row]
        if !imageData.isReadOnly {
            let display = PhotoListAlertView()
            display.delegate = self
            display.displayAlertMessage(delegate: controller!, title: "removeImage".localized, row: indexPath)
        }
    }
}
