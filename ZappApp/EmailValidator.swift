//
//  EmailValidator.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 12.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation

class EmailValidator {
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
