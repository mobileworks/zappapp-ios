//
//  Api.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import JWT
import Alamofire
import SwiftyJSON

class Api{
    
    var currentUser : User?{
        get {
            if let sessionUser = UserDefaults.standard.object(forKey: "currentUser") as? NSData{
                if let storedUser = NSKeyedUnarchiver.unarchiveObject(with: sessionUser as Data) as? User{
                    return storedUser
                }
            }
            return nil
        }
    }
    
    private func getClaims() -> NSDictionary{
        let claims = [
            "iss": currentUser?.guid ?? "",
            "aud": "MaestroLIVE",
            "sub":"MaestroLIVE API",
            "exp": (NSDate().timeIntervalSince1970 + (60.0*60.0)),
            "iat": (NSDate().timeIntervalSince1970 - 3600)
            ] as [String : Any]
        return claims as NSDictionary
    }
    
    func generateToken(secretKey: String) -> String{
        let token = JWT.encode(claims: self.getClaims() as! Payload, algorithm: .hs256(secretKey.data(using: .utf8)!))
        return token
    }
    
    func getPayload(response: String, secretKey: String) -> Payload? {
        do {
            let payload = try JWT.decode(response, algorithm: .hs256(secretKey.data(using: .utf8)!))
            return payload
            
        } catch {
            print("Failed to decode JWT: \(error)")
        }
        return nil
    }
    
    func call(_ JWTenabled:Bool, method: HTTPMethod, endpoint: String, content: Parameters?, params: Parameters?, senderParam: Int, queue: DispatchQueue, completion:@escaping (_ finished: Bool, _ response: JSON?) ->Void) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let savedInstance = appDelegate.savedInstance
        
        if savedInstance != "" {
            var finalUrl = savedInstance + endpoint
            var finalParams = (params == nil) ? content : params
            
            let secretKey = currentUser?.secretKey
            
            if JWTenabled && secretKey != nil {
                finalParams![SyncConsts.TOKEN] = self.generateToken(secretKey: secretKey!)
            }
            
            print(finalUrl)
            
            if method == HTTPMethod.get {
                Alamofire.request(finalUrl, method: method, parameters: finalParams, encoding: URLEncoding.default).responseJSON(queue: queue, completionHandler: {
                    response in
                    if let responseResult = response.result.value{
                        let responseJson = JSON(responseResult)
                        print(response.request ?? "")
                        print("Request size \(Float((response.data?.count)!/1024)) kB")
                        if JWTenabled && secretKey != nil {
                            if let payload = self.getPayload(response: responseJson[SyncConsts.TOKEN].stringValue, secretKey: secretKey!){
                                completion(true, JSON(payload[SyncConsts.RESPONSE] ?? "json payload"))
                            } else {
                                //self.exceptionsHandling(responseJson, senderParam)
                                completion(false, responseJson)
                            }
                        } else {
                            completion(true, responseJson)
                        }
                    } else {
                        let notJson = response.result.debugDescription
                        print(notJson)
                        completion(false, JSON(notJson))
                    }
                })
            } else  if method == HTTPMethod.post || method == HTTPMethod.put {
                if JWTenabled {
                    if let token = finalParams![SyncConsts.TOKEN]{
                        finalUrl = finalUrl+"?token="+(token as! String)
                    }
                    Alamofire.request(finalUrl, method: method, parameters: content, encoding: JSONEncoding.default).responseJSON(queue: queue, completionHandler: {
                        response in
                        if let responseResult = response.result.value {
                            let responseJson = JSON(responseResult)
                            print(response.request ?? "")
                            if !finalUrl.contains("login") {
                                print(responseJson)
                            }
                            print("Request size \(Float((response.data?.count)!/1024)) kB")
                            if JWTenabled && secretKey != nil {
                                if let payload = self.getPayload(response: responseJson[SyncConsts.TOKEN].stringValue, secretKey: secretKey!){
                                    completion(true, JSON(payload[SyncConsts.RESPONSE] ?? "json payload"))
                                } else {
                                    //self.exceptionsHandling(responseJson, senderParam)
                                    completion(false, responseJson)
                                }
                            }
                        } else {
                            let notJson = response.result.debugDescription
                            print(notJson)
                            completion(false, JSON(notJson))
                        }
                    })
                } else {
                    Alamofire.request(finalUrl, method: method, parameters: finalParams, encoding: JSONEncoding.default).responseJSON(queue: queue, completionHandler: {
                        response in
                        if let responseResult = response.result.value {
                            let responseJson = JSON(responseResult)
                            print(response.request ?? "")
                            print("Request size \(Float((response.data?.count)!/1024)) kB")
                            if JWTenabled && secretKey != nil {
                                if let payload = self.getPayload(response: responseJson[SyncConsts.TOKEN].stringValue, secretKey: secretKey!){
                                    completion(true, JSON(payload[SyncConsts.RESPONSE] ?? "json payload"))
                                } else {
                                    //self.exceptionsHandling(responseJson, senderParam)
                                    completion(false, responseJson)
                                }
                            } else {
                                // login case
                                if responseJson[SyncConsts.SUCCESS].exists() {
                                    if responseJson[SyncConsts.SUCCESS].boolValue {
                                        completion(true, responseJson)
                                    } else {
                                        let message = ["message":responseJson[SyncConsts.MESSAGE].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))]
                                        //NotificationCenter.default.post(name: Notification.Name(rawValue: LoginViewController.showLoginErrorMessage), object: message)
                                        //completion(false, responseJson)
                                    }
                                }
                            }
                        } else {
                            let notJson = response.result.debugDescription
                            print(notJson)
                            completion(false, JSON(notJson))
                        }
                    })
                }
            }
        }
    }
}
