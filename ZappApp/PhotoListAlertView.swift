//
//  PhotoListAlertView.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 26.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit

protocol PhotoListAlertDelegate: class {
    func getValue(row: IndexPath)
}

class PhotoListAlertView {
    weak var delegate: PhotoListAlertDelegate?
    
    func displayAlertMessage(delegate: UIViewController, title: String, row: IndexPath) {
        let alertController = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        
        let alertActionYes = UIAlertAction(title: "yes".localized, style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) in
            self.delegate?.getValue(row: row)
        }
        
        let alertActionNo = UIAlertAction(title: "no".localized, style: UIAlertAction.Style.default, handler: nil)
        
        alertController.addAction(alertActionNo)
        alertController.addAction(alertActionYes)
        
        delegate.present(alertController, animated:true, completion:nil)
    }
}
