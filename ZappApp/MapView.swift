//
//  MapView.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 28.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class MapView: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager = CLLocationManager()
    var location: CLLocation? = nil

    let distanceSpan: Double = 250
    var mapView: MKMapView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView = MKMapView()
        
        mapView?.mapType = MKMapType.standard
        mapView?.isZoomEnabled = true
        mapView?.isScrollEnabled = true
        mapView?.center = view.center
        mapView?.delegate = self
        
        view.addSubview(mapView!)
        
        self.locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.requestLocation()
        }
        
        let leftMargin:CGFloat = 0
        let topMargin:CGFloat = view.safeAreaInsets.top
        let mapWidth:CGFloat = view.frame.size.width
        let mapHeight:CGFloat = view.frame.size.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom
        
        mapView!.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        mapView!.register(MapPoint.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        let realm = try! Realm()
        
        let docs = Document.getDocumentsByType(type: DocTypeConsts.MC3_ENQUIRY, realm)
        for document in docs {
            
            var address : String? = ""
            var enquiryType: String? = ""
            var created : String? = ""
            var location2d: CLLocationCoordinate2D? = nil
            var statusPhoto = UIImage()
            
            let properties = DocumentProperty.getDocumentPropertiesByDocumentType(docType: DocTypeConsts.MC3_ENQUIRY, realm)
            
            for property in properties {
                switch property.key! {
                case PropertiesConsts.ADDRESS:
                    if let add = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                        address = add.content
                    }
                case PropertiesConsts.CREATED:
                    if let createdStr = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                        if let date = createdStr.contentDate {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                            let strDate = dateFormatter.string(from: date)
                            created = "Created: \(strDate)"
                        }
                    }
                case PropertiesConsts.ENQUIRY_TYPE:
                    if let enquiry = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                        if !enquiry.documents.isEmpty {
                            if let enquiryTypeDoc = enquiry.documents.first {
                                if enquiryTypeDoc.guid != nil {
                                    enquiryType = DocumentParser.getContentFromDp(docType: DocTypeConsts.MC3_ENQUIRY_TYPE, property: PropertiesConsts.NAME, docGuid: enquiryTypeDoc.guid!, realm)
                                }
                            }
                        }
                    }
                case PropertiesConsts.STATUS:
                    if let enquiryStatusGuid = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                        if !enquiryStatusGuid.documents.isEmpty {
                            if let enquiryStatusDoc = enquiryStatusGuid.documents.first {
                                if enquiryStatusDoc.guid != nil {
                                    if let status = DocumentParser.getContentFromDp(docType: DocTypeConsts.MC3_ENQUIRY_STATUS, property: PropertiesConsts.KEY, docGuid: enquiryStatusDoc.guid!, realm) {
                                        switch status {
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_NEW:
                                            statusPhoto = UIImage(named: "New")!
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_OPEN:
                                            statusPhoto = UIImage(named: "Open")!
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_CLOSED:
                                            statusPhoto = UIImage(named: "Resolved")!
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_REASSIGN:
                                            statusPhoto = UIImage(named: "Reassign")!
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_ATTENDING_TO_SOLVE_THE_ISSUE:
                                            statusPhoto = UIImage(named: "Attending to solve")!
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_ASSIGNED:
                                            statusPhoto = UIImage(named: "Assigned")!
                                        case PropertiesConsts.MC3_ENQUIRY_STATUS_DELETED:
                                            statusPhoto = UIImage(named: "Deleted")!
                                        default:
                                            statusPhoto = UIImage(named: "New")!
                                        }
                                    }
                                }
                            }
                        }
                    }
                case PropertiesConsts.LOCATION:
                    if let location = DocumentPropertyValue.getDocumentPropertyValueByPropertyId(propertyId: property.id, properties: document.propertyValues) {
                        if (location.content != nil) {
                            if let loc = DocumentParser.getLocationCordinate(location.content!) {
                                location2d = CLLocationCoordinate2D(latitude: loc.latitude, longitude: loc.longitude)
                            }
                        }
                    }
                default:
                    break
                }
            }
            
            if (address != nil && enquiryType != nil && created != nil && location2d != nil) {
                let annotation = MapPoint(address: address!, enquiryType: enquiryType!, date: created!, coordinate: location2d!, image: statusPhoto)
                mapView?.addAnnotation(annotation)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied:
            let alert = UIAlertController(title: "locationServicesDisabled".localized, message: "locationAccessRequired".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "allowLocation".localized, style: .cancel, handler: { (alert) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }

                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)
                }
            }))
            present(alert, animated: true, completion: nil)
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let mapView = self.mapView {
            if let location = locations.last {
                let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: self.distanceSpan, longitudinalMeters: self.distanceSpan)
                mapView.setRegion(region, animated: true)
                mapView.showsUserLocation = true
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MapPoint else { return nil }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)  as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            
            let mainView = UIStackView()
            mainView.translatesAutoresizingMaskIntoConstraints = false
            mainView.axis = .horizontal
            mainView.distribution = .equalSpacing
            mainView.alignment = .fill
            mainView.spacing = 8

            let subView = UIStackView()
            subView.translatesAutoresizingMaskIntoConstraints = false
            subView.axis = .vertical
            subView.distribution = .equalSpacing
            subView.alignment = .top
            subView.spacing = 8
            
            let addressTextField = UILabel()
            addressTextField.translatesAutoresizingMaskIntoConstraints = false
            addressTextField.text = annotation.address
            subView.addArrangedSubview(addressTextField)
            
            let enquiryTextField = UILabel()
            enquiryTextField.translatesAutoresizingMaskIntoConstraints = false
            enquiryTextField.text = annotation.enquiryType
            subView.addArrangedSubview(enquiryTextField)

            let dateTextField = UILabel()
            dateTextField.translatesAutoresizingMaskIntoConstraints = false
            dateTextField.text = annotation.date
            subView.addArrangedSubview(dateTextField)
            
            let imageView = UIImageView()
            imageView.backgroundColor = UIColor.blue
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.frame = CGRect(x: 0, y: 0, width: subView.bounds.size.height, height: subView.bounds.size.height)
            imageView.image = annotation.image
            
            mainView.addArrangedSubview(subView)
            mainView.addArrangedSubview(imageView)

            view.detailCalloutAccessoryView = mainView
        }
        return view
    }
}



