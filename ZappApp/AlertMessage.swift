//
//  AlertMessage.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation
import UIKit

class AlertMessage{
    static func displayAlertMessage(_ delegate: UIViewController, title:String, message:String, actionLabel:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: actionLabel, style: UIAlertAction.Style.default, handler: nil)
        
        alertController.addAction(alertAction)
        
        delegate.present(alertController, animated:true, completion:nil)
    }
    
    static func displayAlertMessage(_ delegate: UIViewController, title:String, message:String, actionLabel:String, uiView: UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: "ok".localized, style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) in
//            delegate.dismiss(animated: true, completion: {
                delegate.present(uiView, animated: true, completion: nil)
//            })
        }
        
        alertController.addAction(alertAction)
        
        delegate.present(alertController, animated:true, completion:nil)
    }
}
