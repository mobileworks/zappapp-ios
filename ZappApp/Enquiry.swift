//
//  Enquiry.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 03.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit

class Enquiry {
    
    //Model for list
    
    // MARK: Properties
    var enquiryAddress: String
    var enquiryTypeName: String
    var created: String
    var enquiryStatusPhoto: UIImage
    
    // MARK: Initialization
    
    init?(enquiryAddress: String?, enquiryTypeName: String?, created: String?, enquiryStatusPhoto: UIImage) {
        // Initialize stored properties.
        self.enquiryAddress = enquiryAddress ?? ""
        self.enquiryTypeName = enquiryTypeName ?? ""
        self.created = created ?? ""
        self.enquiryStatusPhoto = enquiryStatusPhoto
    }
}
