//
//  EnquiryListCell.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 02.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit

class EnquiryListCell: UITableViewCell {
    
    //MARK: Properties
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var enquiryType: UILabel!
    @IBOutlet weak var created: UILabel!
    @IBOutlet weak var enquiryStatusImage: UIImageView!
}
