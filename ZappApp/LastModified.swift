//
//  LastModified.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 22.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit
import Alamofire

class LastModified {
    
    // MARK: Properties
    var currentLastModified: Int64
    var serverLastModified: Int64
    var noOfResults: Int
    
    // MARK: Initialization
    
    init?(currentLastModified: Int64, serverLastModified: Int64, noOfResults: Int) {
        // Initialize stored properties.
        self.currentLastModified = currentLastModified
        self.serverLastModified = serverLastModified
        self.noOfResults = noOfResults
    }
}
