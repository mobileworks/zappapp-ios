//
//  Login.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Alamofire
import SwiftyJSON

class ApiLogin : Api{
    
    func call(params: Parameters, queue: DispatchQueue, completion:@escaping (_ finished: Bool, _ response: JSON?) -> Void) {
        super.call(false, method: HTTPMethod.post, endpoint: Config.API.Operations.login.endpoint, content: nil, params: params, senderParam: 0, queue: queue, completion: completion)
    }
}
