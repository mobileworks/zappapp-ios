//
//  SyncConsts.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 13.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation

public class SyncConsts {
    //params
    static let LAST_MODIFIED = "lastModified"
    static let START_FROM = "startFrom"
    static let MAX_RESULTS = "maxResults"
    static let TYPE = "type"
    static let TYPES = "types"
    static let PROPERTIES = "properties"
    static let ID = "id"
    static let FILTERS = "filters"
    static let SORT = "sort"
    static let NO = "no"
    static let IS_DELETED = "isDeleted"
    static let IS_COMPLETED = "isCompleted"
    static let GUID = "guid"
    static let IGNORE_DELETED = "ignoreDeleted"
    static let EMAIL = "email"
    static let MOBILE = "mobile"
    
    static let SUCCESS = "success"

    static let API_V2_1 = "/api/v2.1/"
    static let API_V2 = "/api/v2/"

    static let DOCUMENT_KML = API_V2_1 + "document.kml"
    static let DOCUMENT_JSON = API_V2_1 + "document.json"
    static let DOCUMENT_COUNT_JSON = API_V2 + "documentCount.json"
    static let DOCUMENT_LAST_MODIFIED_JSON = API_V2 + "documentLastModified.json"
    static let INSTANCES_JSON = API_V2 + "instances.json"
    static let LOGIN_JSON = API_V2 + "login.json"

    static let ASSETS = "assets" //assets flag
    static let SKIP_SYNC = "skipSync" //skip normal sync flag
    static let SKIP_PERSIST = "skipPersist" //don't download at all flag
    static let PREV_ASSIGNEES = "prevAssignees" //download only documents assigned to
    static let PREV_ASSIGNEES_SKIP_SYNC = "prevAssigneesSkipSync" //download only documents assigned to and attached ones
    
    static let ACTIONS = "actions"
    static let ACTION = "action"
    static let AVAILABLE_ACTIONS = "availableActions"
    static let LIST_PROPERTY = "listProperty"
    static let LIST_NAME = "listName"
    static let PERMISSIONS = "permissions"
    static let PROPERTIES_VALUES = "propertiesValues"
    static let DOCUMENT = "document"
    static let STOPALERTLEVELS = "stopalertlevels"
    static let STOPALERTS = "stopAlerts"
    static let DOCUMENTS = "documents"
    static let COUNT = "count"
    static let MAESTROLIVE = "MaestroLIVE"
    static let MAESTROLIVE_MANGO = "MaestroLIVE Mango"
    static let TOKEN = "token"
    static let RESPONSE = "response"
    static let EXCEPTION = "exception"
    static let ERRORS = "errors"
    static let MESSAGE = "message"
    static let CODE = "code"
    static let NAME = "name"
    static let CREATED = "created"
    static let UPDATED = "updated"
    
    static let URL = "url"
    static let INSTANCES = "instances"

    static let MOBILE_USER = "mobile user"
    static let RESULT = "result"
    static let VALIDATION_ERRORS = "validationErrors"
}
