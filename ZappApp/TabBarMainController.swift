//
//  TabBarMainController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 28.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import RealmSwift
import UIKit

class TabBarMainController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let tabView = storyboard.instantiateViewController(withIdentifier: "EnquiryListController")
        tabView.tabBarItem = UITabBarItem(title: "My Zapps", image: UIImage(named: "List"), selectedImage: UIImage(named: "List"))
        self.viewControllers = [tabView]
        
        addTabBarItem(viewController: MapView(), title: "Map", image: UIImage(named: "Map"), selectedImage: UIImage(named: "Map"))

        let tabView2 = storyboard.instantiateViewController(withIdentifier: "NewEnquiry")
        tabView2.tabBarItem = UITabBarItem(title: "New Zapp", image: UIImage(named: "Add"), selectedImage: UIImage(named: "Add"))
        tabView2.modalPresentationStyle = .overCurrentContext
        self.viewControllers?.append(tabView2)
        
//        if let viewController = storyboard.instantiateViewController(withIdentifier: "NewEnquiry") as? UIViewController {
//            viewController.tabBarItem = UITabBarItem(title: "New Zapp", image: UIImage(named: "Add"), selectedImage: UIImage(named: "Add"))
//            if let navigator = navigationController {
//                navigator.pushViewController(viewController, animated: true)
//            }
//        }
    }
    
    func addTabBarItem(viewController: UIViewController, title: String, image: UIImage?, selectedImage: UIImage?) {
        
        let nav = UINavigationController()
        nav.viewControllers = [viewController]
        nav.tabBarItem = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        
        self.viewControllers?.append(nav)
    }
}
