//
//  EnquiryProblemViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 05.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import RealmSwift

class EnquiryProblemViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var pickerData: [PickerViewObject] = [PickerViewObject]()
    var enquiryTypeGuid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.problemPicker.delegate = self
        self.problemPicker.dataSource = self
        
        self.nextButton.isEnabled = false;
        
        var array : [PickerViewObject] = []
        array.append(PickerViewObject(key: "" , value: "Select enquiry type")!)
        
        let documents = Document.getDocumentsByType(type: DocTypeConsts.MC3_ENQUIRY_TYPE, try! Realm())
        for document in documents {
            array.append(PickerViewObject(key: document.guid!, value: document.listName!)!)
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.enquiry = NewEnquiry()
        
        pickerData = array
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var problemPicker: UIPickerView!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    // MARK: - Navigation
    
    // The number of columns of data   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].value
    }
    
    // Catpure the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row > 0 {
            nextButton.isEnabled = true;
            enquiryTypeGuid = pickerData[row].key
        } else {
            nextButton.isEnabled = false;
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        present(TabBarMainController(), animated: true, completion: nil)
    }
    
    @IBAction func nextButton(_ sender: UIBarButtonItem) {
        saveEnquiry(enquiryTypeGuid: enquiryTypeGuid)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnquiryPictureController")
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func saveEnquiry(enquiryTypeGuid : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let enquiry = appDelegate.enquiry
        enquiry.enquiryTypeName = enquiryTypeGuid
    }
}
