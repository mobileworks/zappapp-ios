//
//  Constants.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 17.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation

class Constants {
    
    static let BODY = "body"
    static let CONTENT = "content"
    static let KEY = "key"
    static let VALUE = "value"
    static let GUID = "guid"
    static let GROUP = "group"
    static let NAME = "name"
    static let TRANSLATION = "translation"
    static let GROUPS = "groups"
    static let PROPERTIES = "properties"
    static let RULESET = "ruleset"
    static let RULES = "rules"
    static let MOBILE = "mobile"
    static let DOCUMENT_TYPE = "documentType"
    static let DYNAMIC = "dynamic"
    static let CONTENT_TYPE = "contentType"
    static let PARAMS = "params"
    static let CLASS_NAME = "className"
    static let TYPE_NAMES = "typeNames"
    static let ENUMS = "enums"
    static let FILTERS = "filters"
    static let LIST_PROPERTY = "listProperty"
    static let LAST_MODIFIED = "lastModified"
    
    static let COMPUTED_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
    static let PHOTO_DATETIME_FORMAT = "yyyy_MM_dd_HH_mm_ss"
}
