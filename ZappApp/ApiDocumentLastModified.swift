//
//  ApiDocumentType.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//


import Alamofire
import JWT
import SwiftyJSON

class ApiDocumentLastModified: Api{
   
    func getLastModified(documentTypesToLoad: [String], queue: DispatchQueue, completion:@escaping (_ finished: Bool, _ response: JSON?) ->Void) {
        var params = [String: String]()
        params[PropertiesConsts.TYPES] = JSON(documentTypesToLoad).rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
        
        super.call(true, method: HTTPMethod.get, endpoint: Config.API.Operations.documentLastModified.endpoint, content: nil, params: params, senderParam: 0, queue: queue, completion:completion)
    }
}
