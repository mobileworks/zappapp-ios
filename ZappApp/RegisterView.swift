//
//  RegisterView.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 28.02.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Alamofire

class RegisterView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate {

    //MARK: Properties
    @IBOutlet weak var titlePicker: UIPickerView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var telephoneNoTextField: UITextField!
    @IBOutlet weak var PINTextField: UITextField!
    
    let DEFAULT_USER = "guest"
    let DEFAULT_PASSWORD = "JustADummyPassword"
    
    var userTitle : String = ""
    var userFirstName : String = ""
    var userLastName : String = ""
    var userPhoneNo : String = ""
    var userMail : String = ""
    var userPIN : String = ""
    var reporterGuid : String = ""
    
    var pickerData: [String] = [String]()
    let activityIndicator: ActivityIndicator = ActivityIndicator()
    
    @IBOutlet weak var scroller: UIScrollView!
    @IBOutlet var viewer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        activityIndicator.setupForView(self.view)
        self.titlePicker.delegate = self
        self.titlePicker.dataSource = self
        scroller.delegate = self
        scroller.isScrollEnabled = true
        
        let realm = try! Realm()
        if let titleProperty = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: DocTypeConsts.CRM_CONTACT, key: PropertiesConsts.TITLE, realm) {
            pickerData = getEnumData(titleProperty, realm: realm)
        } else {
            pickerData = ["Ms", "Mrs", "Mr", "Miss", "Dr", "Mr & Mrs", "Cllr", "Sir"]
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func getEnumData(_ property: DocumentProperty, realm: Realm) -> [String] {
        
        var labels = [String]()
        
        if let dataFromString = property.params?.data(using: .utf8, allowLossyConversion: false) {
            do {
                let json = try JSON(data: dataFromString)
                
                let groupsArray:[String] = json.arrayValue.map { $0.string!}
                for object in groupsArray {
                    if object == "" {
                        labels.append("   ")
                    } else {
                        labels.append(object)
                    }
                }
            } catch {
                print(error)
            }
        }
        return labels
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let height = viewer.bounds.size.height + ((notification.name == UIResponder.keyboardWillShowNotification) ? keyboardHeight.height : 0)
            scroller.contentSize = CGSize(width: viewer.bounds.size.width, height: height)
        }
    }
    
    //MARK: PickerViewDelegate
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    //MARK: Actions
    
    @IBAction func registerAction(_ sender: UIButton) {
        
        var cancel = false
        var cancelLabel = ""
        
        userFirstName = nameTextField.text!
        userLastName = lastNameTextField.text!
        userPIN = PINTextField.text!
        userMail = emailTextField.text!
        userPhoneNo = telephoneNoTextField.text!
        
        // Check if title was choosen
        userTitle = pickerData[titlePicker.selectedRow(inComponent: 0)]
        if titlePicker.selectedRow(inComponent: 0) == 0 {
            titlePicker.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "title not chosen, \n"
        } else {
            titlePicker.layer.borderColor = UIColor.clear.cgColor
        }
        
        // Check for a valid user first name.
        if userFirstName.isEmpty {
            nameTextField.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "first name is empty, \n"
        } else {
            nameTextField.layer.borderColor = UIColor.clear.cgColor
        }
        
        
        // Check for a valid user last name.
        if userLastName.isEmpty {
            lastNameTextField.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "last name is empty, \n"
        } else {
            lastNameTextField.layer.borderColor = UIColor.clear.cgColor
        }
        
        
        // Check for a valid user PIN.
        if userPIN.isEmpty {
            PINTextField.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "PIN is empty, \n"
        } else {
            if userPIN.count < 4 {
                PINTextField.layer.borderColor = UIColor.red.cgColor
                cancel = true
                cancelLabel += "PIN is too short, \n"
            }
            PINTextField.layer.borderColor = UIColor.clear.cgColor
        }
        
        // Check for a valid user email.
        if userMail.isEmpty {
            emailTextField.layer.borderColor = UIColor.red.cgColor
            cancel = true
            cancelLabel += "email is empty, \n"
        } else {
            if (!(EmailValidator.isValidEmail((emailTextField.text)!))) {
                emailTextField.layer.borderColor = UIColor.red.cgColor
                cancel = true
                cancelLabel += "email is in wrong format, \n"
            }
        }

        if cancel {
            AlertMessage.displayAlertMessage(self, title: "fieldsWronglyFilled".localized, message: cancelLabel, actionLabel: "ok".localized)
        } else {
            atempt2Register()
        }
    }
        
    func atempt2Register() {
        
        let parameters: Parameters = [
            "username": DEFAULT_USER,
            "password": DEFAULT_PASSWORD
        ]
        
        nameTextField.resignFirstResponder()
        
        activityIndicator.start()
        let loginThread = DispatchQueue(label: "login", qos: .background, target: nil)
        loginThread.async {
            ApiLogin().call(params: parameters, queue: loginThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished{
                    if (response?[SyncConsts.SUCCESS].boolValue)! {

                        RegisterView.getJSONFromModule(response: response!)
                        CurrentUser().storeUser(userParams: response!["user"])
                        
                        self.checkIfReporterExists(userMail: self.userMail, completion: {status in
                            switch(status) {
                            case 5:
                                DispatchQueue.main.async(execute: {
                                    self.activityIndicator.stop()
                                    AlertMessage.displayAlertMessage(self, title: "registerError".localized, message: "emailValidationErrorWhenCreatingNewUser".localized, actionLabel: "ok".localized)
                                })
                            case 4:
                                DispatchQueue.main.async(execute: {
                                    self.activityIndicator.stop()
                                    AlertMessage.displayAlertMessage(self, title: "registerError".localized, message: "emailValidationErrorWhenCreatingNewUser".localized, actionLabel: "ok".localized)
                                })
                            case 3:
                                DispatchQueue.main.async(execute: {
                                    self.activityIndicator.stop()
                                    AlertMessage.displayAlertMessage(self, title: "registerError".localized, message: "errorWhenRegisteringNewUser".localized, actionLabel: "ok".localized)
                                })
                            case 2:
                                UserDefaults.standard.set(self.userMail, forKey: PropertiesConsts.USER_EMAIL)
                                UserDefaults.standard.set(self.userPIN, forKey: PropertiesConsts.USER_PIN)
                                UserDefaults.standard.set(self.reporterGuid, forKey: PropertiesConsts.REPORTER_GUID)
                                
                                UserDefaults.standard.set(self.userFirstName, forKey: PropertiesConsts.USER_FIRST_NAME)
                                UserDefaults.standard.set(self.userLastName, forKey: PropertiesConsts.USER_LAST_NAME)
                                UserDefaults.standard.set(self.userTitle, forKey: PropertiesConsts.USER_TITLE)
                                UserDefaults.standard.set(self.userPhoneNo, forKey: PropertiesConsts.USER_PHONE_NO)
                                
                                UserDefaults.standard.synchronize()
                                
                                GetDataThread().run(completion: {status in
                                    if status {
                                        DispatchQueue.main.async(execute: {
                                            self.activityIndicator.stop()
                                            self.present(TabBarMainController(), animated: true, completion: nil)
                                        })
                                    } else {
                                        DispatchQueue.main.async(execute: {
                                            self.activityIndicator.stop()
                                            AlertMessage.displayAlertMessage(self, title: "errorWhenDownloadingData".localized, message: "errorWhenCheckingUser".localized, actionLabel: "ok".localized)
                                        })
                                    }
                                })
                            case 1:
                                DispatchQueue.main.async(execute: {
                                    self.activityIndicator.stop()
                                    AlertMessage.displayAlertMessage(self, title: "registerError".localized, message: "thisEmailIsAlreadyConnectedToExistingUser".localized, actionLabel: "OK")
                                })
                            case 0:
                                DispatchQueue.main.async(execute: {
                                    self.activityIndicator.stop()
                                    AlertMessage.displayAlertMessage(self, title: "registerError".localized, message: "errorWhenCheckingUser".localized, actionLabel: "ok".localized)
                                })
                            default:
                                break
                                }
                            }
                        )
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stop()
                            AlertMessage.displayAlertMessage(self, title: "serverSettingsError".localized, message: "pleaseContactAdministrator".localized, actionLabel: "ok".localized)
                        })
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        self.activityIndicator.stop()
                        AlertMessage.displayAlertMessage(self, title: "connectionError".localized, message: "pleaseTryAgain".localized, actionLabel: "ok".localized)
                    })
                }
            })
        }
    }
    
    static func getJSONFromModule(response : JSON) -> [String] {
//        let settings = response[PropertiesConsts.INSTANCE][PropertiesConsts.SETTINGS]
//        resultMap.removeAll()
//        settingsIteration(settingsJSON: settings)
//
//        UserDefaults.standard.setValue(resultMap, forKey: PropertiesConsts.SETTINGS)
//        UserDefaults.standard.synchronize()
        
        var instanceModules = [String]()
        let realm = try! Realm()
        try! realm.write {
            var docTypes = [String]()
            for (_, module):(String, JSON) in response[PropertiesConsts.INSTANCE][PropertiesConsts.MODULES] {
                if let instanceName = module[PropertiesConsts.NAME].string {
                    if instanceName == DocTypeConsts.MC3 || instanceName == DocTypeConsts.CRM {
                        for (_, documentTypeParams):(String, JSON) in module[PropertiesConsts.DOCUMENT_TYPES] {
                            if let dt = documentTypes2Db(documentJSON: documentTypeParams, realm: realm) {
                                docTypes.append(dt)
                            }
                        }
                        instanceModules.append(instanceName)
                    }
                }
            }
            let existingDocTypes = DocumentType.getDocumentTypesAll(realm)
            for existingDocType in existingDocTypes {
                if !docTypes.contains(existingDocType.name!) {
                    print(existingDocType.name! + " does not exist anymore")
                    realm.delete(existingDocType)
                }
            }
        }
        return instanceModules
    }

    static func documentTypes2Db(documentJSON: JSON, realm: Realm) -> String? {
        
        let docType = documentJSON[PropertiesConsts.NAME].stringValue
        var documentType = DocumentType.getDocumentType(docType: docType, realm)
        if documentType == nil {
            documentType = realm.create(DocumentType.self, value: [DocumentType.NAME: docType], update: false)
            
            documentType?.translation = documentJSON[PropertiesConsts.TRANSLATION].stringValue
            documentType?.lastModified = Int64(documentJSON[PropertiesConsts.LAST_MODIFIED].intValue)
            
            if documentJSON[PropertiesConsts.GROUPS].exists() {
                documentType?.groups = documentJSON[PropertiesConsts.GROUPS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))
                let groupsArray:[String] = documentJSON[PropertiesConsts.GROUPS].arrayValue.map { $0.string!}
                for asset in groupsArray {
                    if asset == SyncConsts.ASSETS {
                        documentType?.asset = true
                        break
                    }
                }
            }
            
            if documentJSON[PropertiesConsts.MODULE].exists() {
                documentType?.module = documentJSON[PropertiesConsts.MODULE].stringValue
            }
            documentType?.listProperty = documentJSON[PropertiesConsts.LIST_PROPERTY].stringValue
            
        } else {
            print("DocumentType already exists  = " + (documentType?.name)!)
            documentType?.translation = documentJSON[PropertiesConsts.TRANSLATION].stringValue
            if documentJSON[PropertiesConsts.GROUPS].exists() {
                documentType?.groups = documentJSON[PropertiesConsts.GROUPS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))
                let groupsArray:[String] = documentJSON[PropertiesConsts.GROUPS].arrayValue.map { $0.string!}
                for asset in groupsArray {
                    if asset == SyncConsts.ASSETS {
                        documentType?.asset = true
                        break
                    }
                }
            }
            
            if documentJSON[PropertiesConsts.MODULE].exists() {
                documentType?.module = documentJSON[PropertiesConsts.MODULE].stringValue
            }
            documentType?.listProperty = documentJSON[PropertiesConsts.LIST_PROPERTY].stringValue
        }
        documentType?.properties.append(objectsIn: documentProperty2Db(documentJSON: documentJSON, docType: docType, realm: realm))
        realm.add(documentType!, update: true)
        return documentType!.name!
    }
    
    static func documentProperty2Db(documentJSON: JSON, docType: String, realm: Realm) -> List<DocumentProperty> {
        let list = List<DocumentProperty>()
        
        let documentType = DocumentType.getDocumentType(docType: docType, realm)
        
        var listAll = DocumentProperty.getDocumentPropertiesByDocumentTypeList(docType: docType, realm)
        
        for (_, var documentPropertyParams) : (String, JSON) in documentJSON[PropertiesConsts.PROPERTIES]{
            let key = documentPropertyParams[PropertiesConsts.KEY].stringValue
            
            if let documentProperty = DocumentProperty.getDocumentPropertyByDocumentTypeKey(docType: docType, key: key, realm) {
                documentProperty.name = documentPropertyParams[PropertiesConsts.NAME].stringValue;
                let group = documentPropertyParams[PropertiesConsts.GROUP].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                documentProperty.group = (group == "null") ? nil : group
                documentProperty.guid = documentPropertyParams[PropertiesConsts.GUID].stringValue
                
                let ruleSetJson = documentPropertyParams[PropertiesConsts.RULESET][PropertiesConsts.RULES][PropertiesConsts.MOBILE]
                if ruleSetJson != JSON.null {
                    RuleSet.updateRuleSet(jObject: ruleSetJson, propertyId: "\(documentProperty.id)", realm)
                }
                
                documentProperty.dynamic = documentPropertyParams[PropertiesConsts.DYNAMIC].boolValue
                documentProperty.classNameStr = documentPropertyParams[PropertiesConsts.CONTENT_TYPE][PropertiesConsts.CLASS_NAME].stringValue
                
                for (_, allowedDocType) : (String, JSON) in documentPropertyParams[PropertiesConsts.CONTENT_TYPE][PropertiesConsts.PARAMS][PropertiesConsts.TYPE_NAMES] {
                    if let typeRealm = DocumentType.getDocumentType(docType: allowedDocType.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!, realm) {
                        if !documentProperty.allowedDocumentTypes.contains(typeRealm) {
                            documentProperty.allowedDocumentTypes.append(typeRealm)
                        }
                    } else {
                        documentProperty.allowedDocumentTypes.append(realm.create(DocumentType.self, value: [DocumentType.NAME : allowedDocType.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))], update: true))
                    }
                }
                
                let params = documentPropertyParams[PropertiesConsts.CONTENT_TYPE][PropertiesConsts.PARAMS][PropertiesConsts.ENUMS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                documentProperty.params = (params == "null") ? nil : params
                let filters = documentPropertyParams[PropertiesConsts.FILTERS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                documentProperty.filters = (filters == "null") ? nil : filters
                
                for dp in listAll {
                    if dp.id == documentProperty.id {
                        if listAll.contains(dp) {
                            listAll.remove(at: listAll.index(of: dp)!)
                            break
                        }
                    }
                }
                realm.add(documentProperty, update: true)
            } else {
                let documentProperty = DocumentProperty()
                
                var nextId = realm.objects(DocumentProperty.self).max(ofProperty: DocumentProperty.ID) as Int?
                if nextId == nil {
                    nextId = 1
                } else {
                    nextId = nextId! + 1
                }
                documentProperty.id = nextId!
                documentProperty.documentType = documentType
                documentProperty.key = documentPropertyParams[PropertiesConsts.KEY].stringValue
                documentProperty.name = documentPropertyParams[PropertiesConsts.NAME].stringValue
                let group = documentPropertyParams[PropertiesConsts.GROUP].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                documentProperty.group = (group == "null") ? nil : group
                documentProperty.guid = documentPropertyParams[PropertiesConsts.GUID].stringValue
                
                let ruleSetJson = documentPropertyParams[PropertiesConsts.RULESET][PropertiesConsts.RULES][PropertiesConsts.MOBILE]
                if ruleSetJson != JSON.null {
                    documentProperty.ruleSet = RuleSet.createNewRuleSet(jObject: ruleSetJson, propertyId: "\(documentProperty.id)", realm)
                }
                
                documentProperty.dynamic = documentPropertyParams[PropertiesConsts.DYNAMIC].boolValue
                documentProperty.classNameStr = documentPropertyParams[PropertiesConsts.CONTENT_TYPE][PropertiesConsts.CLASS_NAME].stringValue
                
                for (_, allowedDocType) : (String, JSON) in documentPropertyParams[PropertiesConsts.CONTENT_TYPE][PropertiesConsts.PARAMS][PropertiesConsts.TYPE_NAMES] {
                    if let typeRealm = DocumentType.getDocumentType(docType: allowedDocType.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!, realm) {
                        if !documentProperty.allowedDocumentTypes.contains(typeRealm) {
                            documentProperty.allowedDocumentTypes.append(typeRealm)
                        }
                    } else {
                        documentProperty.allowedDocumentTypes.append(realm.create(DocumentType.self, value: [DocumentType.NAME : allowedDocType.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!], update: true))
                    }
                }
                let params = documentPropertyParams[PropertiesConsts.CONTENT_TYPE][PropertiesConsts.PARAMS][PropertiesConsts.ENUMS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                documentProperty.params = (params == "null") ? nil : params
                let filters = documentPropertyParams[PropertiesConsts.FILTERS].rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions(rawValue: 0))!
                documentProperty.filters = (filters == "null") ? nil : filters
                for dp in listAll {
                    if dp.id == documentProperty.id {
                        if listAll.contains(dp) {
                            listAll.remove(at: listAll.index(of: dp)!)
                            break
                        }
                    }
                }
                realm.add(documentProperty, update: false)
                list.append(documentProperty)
            }
        }
        return list
    }
    
    func checkIfReporterExists(userMail: String, completion:@escaping (_ status : Int) -> ()) {
        var params = Parameters()
        params[SyncConsts.TYPE] = DocTypeConsts.CRM_CONTACT
        params[SyncConsts.LAST_MODIFIED] = "0"
        params[SyncConsts.MAX_RESULTS] = "10"
        params[SyncConsts.PROPERTIES] = JSON([String: AnyObject]()).rawString()
        
        let json : JSON = [PropertiesConsts.EMAIL: userMail]
        params[SyncConsts.FILTERS] = json.rawString()
        
        let checkThread = DispatchQueue(label: "check", qos: .background, target: nil)
        checkThread.async {
            ApiDocument().getList(params: params, endPoint: Config.API.Operations.document.endpoint, queue: checkThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished{
                    for (key, doc):(String, JSON) in response! {
                        if key == SyncConsts.DOCUMENTS {
                            let array = doc.arrayValue
                            if array.count == 0 {
                                self.addNewReporter(completionAdd: { statusAdd in
                                    completion(statusAdd)
                                })
                            } else {
                                completion(1)
                            }
                        }
                    }
                } else {
                    completion(0)
                }
            })
        }
    }
    
    func addNewReporter(completionAdd:@escaping (_ statusAdd : Int) -> ()) -> () {

        var properties = [Any]()
        
        let realm = try! Realm()
        for property in DocumentProperty.getDocumentPropertiesByDocumentType(docType: DocTypeConsts.CRM_CONTACT, realm) {
            switch property.key! {
            case PropertiesConsts.TITLE:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: self.userTitle)
            case PropertiesConsts.FIRST_NAME:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: self.userFirstName)
            case PropertiesConsts.LAST_NAME:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: self.userLastName)
            case PropertiesConsts.PIN_CODE:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: self.userPIN)
            case PropertiesConsts.EMAIL:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: self.userMail)
            case PropertiesConsts.PHONE_NUMBER:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: self.userPhoneNo)
            case PropertiesConsts.PUBLIC_REPORTER:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: 1)
            case PropertiesConsts.MOBILE_CONTACT:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: 1)
            case PropertiesConsts.THIRD_PARTY:
                properties = DocumentParser.add2PropertiesArray(properties, key: property.key!, value: 0)
            default:
                break
                //print(property.key)
            }
        }

        var json = Parameters()
        json[PropertiesConsts.TYPE] = DocTypeConsts.CRM_CONTACT
        json[PropertiesConsts.TIME] = NSDate().timeIntervalSince1970
        json[PropertiesConsts.PROPERTIES] = properties
        
        let addReporterThread = DispatchQueue(label: "addReporter", qos: .background, target: nil)
        addReporterThread.async {
            ApiDocument().create(content: json, endPoint: Config.API.Operations.document.endpoint, senderParam: 0, queue: addReporterThread, completion:{ (finished: Bool, response: JSON?) -> Void in
                if finished {
                    for (key, doc):(String, JSON) in response! {
                        if key == SyncConsts.DOCUMENT {
                            if let repGuid = doc[SyncConsts.GUID].string {
                                self.reporterGuid = repGuid
                                completionAdd(2)
                            } else {
                                completionAdd(3)
                            }
                        } else if key == SyncConsts.VALIDATION_ERRORS {
                            if doc[SyncConsts.EMAIL] != JSON.null {
                                completionAdd(4)
                            } else {
                                completionAdd(5)
                            }
                        }
                    }
                }
            })
        }
    }
}
