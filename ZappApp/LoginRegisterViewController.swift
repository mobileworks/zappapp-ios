//
//  FirstViewController.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 21.02.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

class LoginRegisterViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var registerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loginView.isHidden = false;
        self.registerView.isHidden = true;
        self.segmentedControl.selectedSegmentIndex = 0;
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            registerView.isHidden = true
            loginView.isHidden = false
        case 1:
            registerView.isHidden = false
            loginView.isHidden = true
        default:
            break
        }
    }
}

