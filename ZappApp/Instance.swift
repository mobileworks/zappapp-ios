//
//  Enquiry.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 03.04.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import UIKit

class Instance {    
    
    // MARK: Properties
    var guid: String
    var key: String
    var url: String
    var name: String
    
    // MARK: Initialization
    
    init?(guid: String, key: String, url: String, name: String) {
        // Initialize stored properties.
        self.guid = guid
        self.key = key
        self.url = url
        self.name = name
    }
}
