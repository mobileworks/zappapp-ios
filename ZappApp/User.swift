//
//  User.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 11.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation
import SwiftyJSON

class User : NSObject, NSCoding{
    
    
    // MARK: Properties
    var guid: String?
    var userName: String?
    var displayName: String?
    var secretKey: String?
    var workerGuid: String?
    
    static let GUID = "guid"
    static let USER_NAME = "username"
    static let DISPLAY_NAME = "displayName"
    static let SECRET_KEY = "secretKey"
    static let WORKER_GUID = "workerGuid"
    
    // MARK: Methods
    
    /**
     Initializes `User` object with given parameters
     
     - parameter userParams:         JSON object with all user params required for initialization
     */
    init(guid: String, userName: String, displayName: String, secretKey: String, workerGuid: String){
        self.guid = guid
        self.userName = userName
        self.displayName = displayName
        self.secretKey = secretKey
        self.workerGuid = workerGuid
    }
    
    @objc func encode(with aCoder: NSCoder) {
        aCoder.encode(self.guid, forKey: User.GUID)
        aCoder.encode(self.userName, forKey: User.USER_NAME)
        aCoder.encode(self.displayName, forKey: User.DISPLAY_NAME)
        aCoder.encode(self.secretKey, forKey: User.SECRET_KEY)
        aCoder.encode(self.workerGuid, forKey: User.WORKER_GUID)
    }
    
    @objc required init(coder aDecoder: NSCoder) {
        super.init()
        self.guid = aDecoder.decodeObject(forKey: User.GUID) as? String
        self.userName = aDecoder.decodeObject(forKey: User.USER_NAME) as? String
        self.displayName = aDecoder.decodeObject(forKey: User.DISPLAY_NAME) as? String
        self.secretKey = aDecoder.decodeObject(forKey: User.SECRET_KEY) as? String
        self.workerGuid = aDecoder.decodeObject(forKey: User.WORKER_GUID) as? String
    }
    
}
