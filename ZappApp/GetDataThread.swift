//
//  GetDataThread.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 18.03.2016.
//  Copyright © 2016 Marek Jarzynski. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class GetDataThread {
    
    let realm = try! Realm()
    var docTypeMap : [String:Int] = [:]
    var docTypeArray = [String]()
    
    var documentTypesMap: [String:Int64] = [:]
    var documentTypesMapToBeUpdated : [String: LastModified] = [:]
    var docTypesToLastModified = [Int:[String]]()

    var lastNoOfDocument: Int = 0
    
    static var isSyncFinished = false
    static var syncThread : DispatchQueue? = nil
    
    
    func run(completion:@escaping (_ status : Bool) -> ()) {
        
        GetDataThread.isSyncFinished = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        GetDataThread.syncThread = DispatchQueue(label: "thread", qos: .background, target: nil)
        //queueMessageLoop to add
        GetDataThread.syncThread?.async {
            autoreleasepool {
                let realm = try! Realm()
//                appDelegate.checkedDocumentTypesCounter = 0
//                appDelegate.messagesQueueCount = Int(Queue.getAllQueueCount(realm))
                
                self.documentTypesMap = [:]
                self.documentTypesMapToBeUpdated = [:]
                
                self.docTypesToLastModified = [Int:[String]]()
                
                var modules = [String]()
                modules.append(DocTypeConsts.CRM)
                modules.append(DocTypeConsts.MC3)                
                
                self.docTypesToLastModified.removeAll()
                self.documentTypesMap.removeAll()
                
                try! realm.write {
                    let docsCount = Document.getAllDocumentCount(realm)
                    if docsCount == 0 {
//                        AppDelegate.isInitialLogin = true
                    }
                    
                    var counter = 0
                    let limit = 30
                    
                    for documentType in DocumentType.getDocumentTypes(modules: modules, realm) {
                        if documentType.name != "" {
                            var add2CheckedDocumentTypes: Bool = true
                            
                            let dt = documentType.groups
                            if let dataFromString = dt?.data(using: .utf8, allowLossyConversion: false) {
                                do {
                                    let json = try JSON(data: dataFromString)
                                    let groupsArray:[String] = json.arrayValue.map { $0.string! }
                                    for asset in groupsArray {
                                        if asset == SyncConsts.SKIP_SYNC || asset == SyncConsts.SKIP_PERSIST {
                                            add2CheckedDocumentTypes = false
                                            break
                                        }
                                    }
                                } catch {
                                    print(error)
                                }
                            }
                            
                            if add2CheckedDocumentTypes {
                                let batch = counter/limit
                                
                                if self.docTypesToLastModified.count <= batch {
                                    self.docTypesToLastModified[batch] = [String]()
                                }
                                self.docTypesToLastModified[batch]?.append(documentType.name!)
                                counter += 1
                            }
                        }
                    }
                }
                if self.docTypesToLastModified.count > 0 && !GetDataThread.isSyncFinished {
                    if let docTypeArray = self.docTypesToLastModified.first {
                        self.lastModifiedLoop(list: docTypeArray.value, syncThread: GetDataThread.syncThread!, appDelegate: appDelegate, completion: {
                            m in completion(m)
                        })
                    }
                } else {
                    completion(true)
                }
                
                DispatchQueue.main.async(execute: {
                    // UI Updates
                })
            }
        }
    }
    
    func lastModifiedLoop(list: [String], syncThread: DispatchQueue, appDelegate: AppDelegate, completion:@escaping (_ status : Bool) -> ()) {
        ApiDocumentLastModified().getLastModified(documentTypesToLoad: list, queue: GetDataThread.syncThread!, completion:{ (finished: Bool, response: JSON?) -> Void in
            if finished {
                for (docType,cnt):(String, JSON) in response! {
                    self.documentTypesMap.updateValue(cnt.int64Value, forKey: docType)
                }
                let realm = try! Realm()
                try! realm.write {
                    for (docType, lastModified) in self.documentTypesMap {
                        if docType != "" {
                            let lastModifiedForDocType = DocumentType.getLastModified(name: docType, realm)
                            let noOfResults = SharedSync.noOfResults / (DocumentType.getDocumentType(docType: docType, realm)?.properties.count)!
                            if lastModified > lastModifiedForDocType! {
                                print("\(docType) last modified from server \(lastModified), last mod from device \(lastModifiedForDocType!)")
                            }
                            if lastModifiedForDocType! < lastModified {
                                self.documentTypesMapToBeUpdated.updateValue(LastModified(currentLastModified: lastModifiedForDocType!, serverLastModified: lastModified, noOfResults: noOfResults)!, forKey: docType)
                            }
                        }
                    }
                }
                self.docTypesToLastModified.remove(at: self.docTypesToLastModified.startIndex)
                
                if self.docTypesToLastModified.count > 0 {
                    if let docTypeArray = self.docTypesToLastModified.first {
                        self.lastModifiedLoop(list: docTypeArray.value, syncThread: syncThread, appDelegate: appDelegate, completion: {
                            m in completion(m)
                        })
                    }
                } else {
                    //self.counter = 1
                    //appDelegate.maxDocumentsTypesCount = self.documentTypesMapToBeUpdated.count
                    
                    if self.documentTypesMapToBeUpdated.isEmpty {
                        // no update to go
                        //self.queueMessagesLoop(syncThread: GetDataThread.syncThread!)
                        completion(true)
                    } else {
                        let group = DispatchGroup()
                        self.iterateLoop(documentTypesMapToBeUpdated: self.documentTypesMapToBeUpdated, syncThread: GetDataThread.syncThread!, group: group)
                        
                        group.notify(qos: DispatchQoS.background, flags: DispatchWorkItemFlags.assignCurrentContext, queue: GetDataThread.syncThread!) {
                            // This closure will be executed when all tasks are complete
                            print("Sync ended !!!")
                            completion(true)
                            //self.queueMessagesLoop(syncThread: GetDataThread.syncThread!)
                        }
                    }
                }
            } else {
                 completion(true)
            }
        })
    }
    
    func wakeUp() {
//        DispatchQueue.main.async(execute: {
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            if !appDelegate.isInSync {
//                SyncThread.timer?.cancel()
//                SyncThread().run()
//            }
//        })
    }
    
    func iterateLoop(documentTypesMapToBeUpdated: [String: LastModified], syncThread: DispatchQueue, group: DispatchGroup) {
        var documentTypesMapToBeUpdated = documentTypesMapToBeUpdated
        let docType = documentTypesMapToBeUpdated.first?.key
        let lm = documentTypesMapToBeUpdated.first?.value
        let realm = try! Realm()
        let docTypeRealm = DocumentType.getDocumentType(docType: docType!, realm)
        group.enter()
        
        self.getDocumentsForDocType(docType: docType!, groups: docTypeRealm?.groups, maxUpdateTs: (lm?.currentLastModified)!, syncThread: syncThread, noOfResults: (lm?.noOfResults)!, group: group, completion:{ status in
            if GetDataThread.isSyncFinished {
                group.leave()
            } else {
                if status {
                    documentTypesMapToBeUpdated.remove(at: documentTypesMapToBeUpdated.startIndex)
                    if documentTypesMapToBeUpdated.count > 0 && !GetDataThread.isSyncFinished {
                        self.iterateLoop(documentTypesMapToBeUpdated: documentTypesMapToBeUpdated, syncThread: syncThread, group: group)
                    }
                    group.leave()
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.checkedDocumentTypesCounter = self.counter
//                    self.counter += 1
                } else {
                    group.leave()
                }
            }
        })
    }
    
    func getDocumentsForDocType(docType: String, groups: String?, maxUpdateTs: Int64, syncThread: DispatchQueue, noOfResults: Int, group: DispatchGroup, completion:@escaping (_ status : Bool) -> ()) {
        
        let sharedSync = SharedSync()
        sharedSync.startSync(type: docType, groups: groups, startFrom: 0, noOfResults: noOfResults, maxUpdateTS: maxUpdateTs, isAsset: false, completion2:  { (result) in
            //document type sync finished
            if result != nil {
                if result!.counterBatch == noOfResults && !GetDataThread.isSyncFinished {
                    let group2 = DispatchGroup()
                    
                    //more data to be downloaded
                    sharedSync.startFromList.append(noOfResults)
                    var totalCounter = noOfResults * 2
                    while result!.counterAll > totalCounter {
                        sharedSync.startFromList.append(totalCounter)
                        totalCounter += noOfResults
                    }
                    //allreadyDownloadedDocs += result.counterBatch
                    
                    var counter = 0
                    var startFromListFrag = [Int]()
                    for startFromItem in sharedSync.startFromList {
                        startFromListFrag.append(startFromItem)
                        sharedSync.startFromList.removeFirst()
                        counter += 1
                        if counter == 4 {
                            break
                        }
                    }
                    self.smartLoop(startListFrom: startFromListFrag, sharedSync: sharedSync, type: docType, groups: groups, maxUpdateTs: maxUpdateTs, syncThread: syncThread, noOfResults: noOfResults, group: group2)
                    
                    group2.notify(qos: DispatchQoS.background, flags: DispatchWorkItemFlags.assignCurrentContext, queue: GetDataThread.syncThread!) {
                        // This closure will be executed when all tasks are complete
                        print("complicated loop ended")
                        completion(true)
                    }
                } else {
                    completion(true)
                }
            } else {
                completion(false)
            }
        })
    }
    
    func smartLoop(startListFrom: [Int], sharedSync: SharedSync, type: String, groups: String?, maxUpdateTs: Int64, syncThread: DispatchQueue, noOfResults: Int, group: DispatchGroup) {
        for startFrom2 in startListFrom {
            let sharedSync2 = SharedSync()
            group.enter()
            sharedSync2.startSync(type: type, groups: groups, startFrom: startFrom2, noOfResults: noOfResults, maxUpdateTS: maxUpdateTs, isAsset: false, completion2:  {(result) in
                group.leave()
                if !sharedSync.startFromList.isEmpty {
                    var startFromList2 = [Int]()
                    startFromList2.append(sharedSync.startFromList.removeFirst())
                    
                    self.smartLoop(startListFrom: startFromList2, sharedSync: sharedSync, type: type, groups: groups, maxUpdateTs: maxUpdateTs, syncThread: syncThread, noOfResults: noOfResults, group: group)
                }
            })
        }
    }
}
