//
//  ImageData.swift
//  ZappApp
//
//  Created by Marek Jarzynski on 26.03.2018.
//  Copyright © 2018 Marek Jarzynski. All rights reserved.
//

import UIKit

class ImageData:Comparable {
    static func <(lhs: ImageData, rhs: ImageData) -> Bool {
        return true
    }
    
    static func ==(lhs: ImageData, rhs: ImageData) -> Bool {
        return lhs.isFromDisk == rhs.isFromDisk &&
            lhs.date == rhs.date &&
            lhs.isReadOnly == rhs.isReadOnly &&
            lhs.photoGuid == rhs.photoGuid &&
            lhs.imageView == rhs.imageView
    }
    
    // MARK: Properties
    var filePath: String? = nil
    var isFromDisk: Bool = false
    var date: String? = nil
    var isReadOnly: Bool = false
    var photoGuid: String? = nil
    var imageView: UIImageView? = nil
    
    // MARK: Initialization
    
    init?(filePath: String, isFromDisk: Bool, date: String, isReadOnly: Bool, photoGuid: String?, imageView: UIImageView) {
        // Initialize stored properties.
        self.filePath = filePath
        self.isFromDisk = isFromDisk
        self.date = date
        self.isReadOnly = isReadOnly
        self.photoGuid = photoGuid
        self.imageView = imageView
    }
    
    init!() {
        
    }
}
